/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/fs.h>
#include <linux/exportfs.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/file.h>
#include <linux/mm_types.h>
#include <linux/smp_lock.h>
#include <linux/gfp.h>
#include <linux/mm.h>
#include <linux/ctype.h>
#include <linux/uaccess.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_CTL_FILE

static void __marshal_raw_dev_list(struct dsfs_dev_list *dl, struct dsfs_dev_list_ *rdl)
{
	int i;

	rdl->magic = cpu_to_be32(DSFS_DEV_LIST_MAGIC);
	rdl->version = cpu_to_be32(DSFS_DEV_LIST_VERSION);
	rdl->dev_count = cpu_to_be32(dl->dev_count);
	for (i = 0; i < dl->dev_count; i++) {
		rdl->dev_list[i].dev_id = cpu_to_be32(dl->dev_list[i].dev_id);
		memcpy(rdl->dev_list[i].dev_name, dl->dev_list[i].dev_name,
		       DSFS_DEV_LIST_MAX_NAME);
	}
}

static int __unmarshal_raw_dev_list(struct dsfs_dev_list_ *rdl, struct dsfs_dev_list *dl)
{
	unsigned int dev_count;
	int i;

	DSFS_DBG("rdl=%p, magic=%08x, version=%08x, dev_count=%d",
		rdl, be32_to_cpu(rdl->magic), be32_to_cpu(rdl->version),
		be32_to_cpu(rdl->dev_count));

	if (be32_to_cpu(rdl->magic) != DSFS_DEV_LIST_MAGIC) {
		printk(KERN_INFO
		       "dsfs: Invalid magic %08x in device list file.\n",
		       be32_to_cpu(rdl->magic));
		return -EINVAL;
	}
	dev_count = be32_to_cpu(rdl->dev_count);
	if (dev_count > DSFS_LAYOUT_MAX_DEVS) {
		printk(KERN_INFO
		       "dsfs: Invalid device count %d in device list file.\n",
		       dev_count);
		return -EINVAL;
	}
	dl->dev_count = dev_count;
	for (i = 0; i < dev_count; i++) {
		dl->dev_list[i].dev_id = be32_to_cpu(rdl->dev_list[i].dev_id);
		memcpy(dl->dev_list[i].dev_name, rdl->dev_list[i].dev_name,
		       DSFS_DEV_LIST_MAX_NAME);
	}
	return 0;
}

static int __write_raw_dev_list(struct file *filp, struct dsfs_dev_list_ *rdl)
{
	mm_segment_t oldfs;
	int err;
	loff_t pos;
	DSFS_DBG("filp=%p, rdl=%p", filp, rdl);

	pos = 0;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = filp->f_op->write(filp, (const char __user *)rdl,
				sizeof(*rdl), &pos);
	set_fs(oldfs);

	return err;
}

static int __read_raw_dev_list(struct file *filp,
			       struct dsfs_dev_list_ *rdl)
{
	mm_segment_t oldfs;
	int err;
	loff_t pos;

	pos = 0;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = filp->f_op->read(filp, (char __user *)rdl, sizeof(*rdl), &pos);
	DSFS_DBG("err=%d, count=%zu", err, sizeof(*rdl));
	set_fs(oldfs);
	return err;
}

/*
 * Initialize the dev list either from an existing DEV_LIST file or
 * creates a default, empty DEV_LIST
 */
static int __init_dev_list(struct dsfs_sb *sbi)
{
	struct dsfs_dev_list_ *rdl;
	struct file *filp;
	int err = 0;
	int cnt;

	rdl = kmalloc(sizeof(*rdl), GFP_KERNEL);
	if (!rdl)
		return -ENOMEM;

	filp = dsfs_open_file(sbi, DSFS_DEV_LIST_FILE);
	if (IS_ERR(filp)) {
		err = PTR_ERR(filp);
		goto out;
	}

	/* Attempt to read the dev list */
	cnt = __read_raw_dev_list(filp, rdl);
	if (cnt == sizeof(*rdl))
		err = __unmarshal_raw_dev_list(rdl, sbi->si_dev_list);

	/*
	 * If there weren't enough bytes in what was read or the
	 * format was bad, recreate a default, valid dev_list
	 */
	if (cnt != sizeof(*rdl) || err) {
		DSFS_DBG("Initializing DEV_LIST cnt=%d, err=%d", cnt, err);
		rdl->magic = cpu_to_be32(DSFS_DEV_LIST_MAGIC);
		rdl->version = cpu_to_be32(DSFS_DEV_LIST_VERSION);
		rdl->dev_count = 0; /* cpu_to_be32(DSFS_LAYOUT_MAX_DEVS); */
		for (cnt = 0; cnt < DSFS_LAYOUT_MAX_DEVS; cnt++)
			rdl->dev_list[cnt].dev_id = cpu_to_be32(-1);
		err = __unmarshal_raw_dev_list(rdl, sbi->si_dev_list);
		BUG_ON(err);
		err = __write_raw_dev_list(filp, rdl);
	}
	filp_close(filp, current->files);
 out:
	kfree(rdl);
	DSFS_DBG("err=%d", err);
	return err;
}

static int dsfs_write_dev_list(struct file *filp, struct dsfs_sb *sbi,
				char __user *buf)
{
	struct dsfs_dev_list_ *rdl;
	loff_t pos = 0;
	int err;
	DSFS_DBG("filp=%p, sbi=%p, buf=%p", filp, sbi, buf);

	if (sbi->si_state != DSFS_INIT)
		return -EINVAL;

	rdl = kmalloc(sizeof(*rdl), GFP_KERNEL);
	if (!rdl)
		return -ENOMEM;

	__marshal_raw_dev_list(sbi->si_dev_list, rdl);
	if (copy_to_user(buf, rdl, sizeof(*rdl))) {
		err = -EACCES;
		goto out;
	}
	DSFS_DBG("filp=%p, buf=%p, pos=%lld, filp->f_op=%p, filp->f_op->write=%p", filp, buf, pos, filp->f_op, filp->f_op->write);
	err = filp->f_op->write(filp, buf, sizeof(*rdl), &pos);
 out:
	kfree(rdl);
	return err;
}

/*
 * Check if the underlying file exists. If it does, process
 * the contents. Otherwise just create an empty dev_list
 */
int dsfs_dev_list_init(struct dentry *root)
{
	struct dentry *dentry;
	int err;
	struct dsfs_sb *sbi = DSFS_SB(root->d_inode->i_sb);
	size_t sz;

	/* Allocate the dev list */
	sbi->si_dev_list = kmalloc(sizeof(struct dsfs_dev_list), GFP_KERNEL);
	if (!sbi->si_dev_list)
		return -ENOMEM;

	/* Lookup the file's dentry */
	dentry = lookup_one_len(DSFS_DEV_LIST_FILE, root,
				strlen(DSFS_DEV_LIST_FILE));
	if (IS_ERR(dentry)) {
		err = PTR_ERR(dentry);
		goto out;
	}
	/* See if the file already exists. If not, create it */
	if (!dentry->d_inode) {
		err = dsfs_create(root->d_inode, dentry, 0644, NULL);
		if (err)
			goto out;
	}
	err = __init_dev_list(sbi);
	DSFS_DBG("err=%d", err);
	if (err > 0)
		err = 0;

	DSFS_I(dentry->d_inode)->ii_type = DSFS_I_DEV_CNTL;
	dentry->d_inode->i_fop = &dsfs_dev_list_fops;
	sz = i_size_read(dsfs_h_inode(dentry->d_inode));
	i_size_write(dentry->d_inode, sz);
 out:
	return err;
}

/*
 * Provide the user with a formatted depiction of the device
 * list. This function does not actually read the file nor update
 * the file's access time.
 */
ssize_t dsfs_dev_list_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	struct file *h_file;
	struct dsfs_sb *sbi;
	int cnt, tot;
	int i;
	char tmp[64];

	DSFS_DBG("file=%p, buf=%p, count=%zu, ppos=%lld", file, buf, count, *ppos);

	/* Any read anywhere except zero returns EOF */
	if (*ppos)
		return 0;

	sbi = DSFS_SB(file->f_dentry->d_inode->i_sb);
	BUG_ON(!sbi);
	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);

	if (!h_file->f_op || !h_file->f_op->read)
		return -ENOSYS;

	tot = cnt = snprintf(tmp, sizeof(tmp),
			     "%-12s : %d\n", "dev_count",
			     sbi->si_dev_list->dev_count);
	if (copy_to_user(buf, tmp, cnt))
		return -EACCES;
	for (i = 0; i < sbi->si_dev_list->dev_count; i++) {
		int dev_id;
		char *name;
		dev_id = sbi->si_dev_list->dev_list[i].dev_id;
		if (dev_id == -1)
			name = "<empty>";
		else
			name = sbi->si_dev_list->dev_list[i].dev_name;
		cnt = snprintf(tmp, sizeof(tmp), "%12d : %s\n",
			       dev_id, name);
		if (copy_to_user(&buf[tot], tmp, cnt))
			return -EACCES;
		tot += cnt;
	}
	file->f_pos = *ppos = tot;
	return tot;
}

static int parse_dev_list_write(struct dsfs_sb *sbi, const char *buf, size_t count)
{
	int cnt, dev;
	char dev_name[DSFS_DEV_LIST_MAX_NAME];

	DSFS_DBG("sbi=%p, count=%zu", sbi, count);
	cnt = sscanf(buf, "set_dev %d %32s", &dev, dev_name);
	DSFS_DBG("cnt=%d", cnt);
	if (cnt == 2) {
		if (dev >= 0 && dev <= sbi->si_dev_list->dev_count) {
			sbi->si_dev_list->dev_list[dev].dev_id = dev;
			memcpy(sbi->si_dev_list->dev_list[dev].dev_name, dev_name,
			       DSFS_DEV_LIST_MAX_NAME);
			if (dev >= sbi->si_dev_list->dev_count)
				sbi->si_dev_list->dev_count = dev + 1;
			goto out;
		}
		goto err;
	}
	cnt = sscanf(buf, "clr_dev %d", &dev);
	DSFS_DBG("cnt=%d", cnt);
	if (cnt == 1) {
		if (dev < DSFS_LAYOUT_MAX_DEVS) {
			sbi->si_dev_list->dev_list[dev].dev_id = -1;
			goto out;
		}
	}
 err:
	DSFS_DBG("parse_error cnt=%d, buf=\"%*.s\"", cnt, (int)count, buf);
	return -EINVAL;

 out:
	return 0;
}
/*
 * Parse formatted strings written to the DEV_LIST file and add or
 * remove devices from the list as requested.
 */
ssize_t dsfs_dev_list_write(struct file *file, const char *buf, size_t count,
			     loff_t *ppos)
{
	int err;
	struct file *h_file;
	struct inode *inode;
	struct inode *h_inode;
	struct dsfs_sb *sbi;
	char cmd[128];
	DSFS_DBG("file=%p, buf=%p, count=%zu, *ppos=%llu",
		file, buf, count, *ppos);

	if (*ppos || count > 128)
		return -EINVAL;

	if (!count)
		return 0;

	DSFS_DBG("buf=%p, count=%zu", buf, count);
	if (copy_from_user(cmd, buf, count))
		return -EACCES;
	
	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);
	if (!h_file->f_op || !h_file->f_op->write)
		return -ENOSYS;

	inode = file->f_dentry->d_inode;
	h_inode = dsfs_h_inode(inode);
	BUG_ON(!h_inode);
	sbi = DSFS_SB(file->f_dentry->d_inode->i_sb);
	BUG_ON(!sbi);
	DSFS_DBG("sbi=%p, cmd=%p, count=%zu", sbi, cmd, count);
	err = parse_dev_list_write(sbi, cmd, count);
	if (err)
		goto out;
	DSFS_DBG("h_file=%p, sbi=%p, err=%d", h_file, sbi, err);
	err = dsfs_write_dev_list(h_file, sbi, (char __user *)buf);
	if (err > 0) {
		dsfs_copy_attr_times(inode, h_inode);
		file->f_pos = *ppos = count;
	}
	/* update this inode's size */
	i_size_write(inode, i_size_read(h_inode));
 out:
	DSFS_DBG("err=%d", err);
	return err;
}

ssize_t dsfs_mds_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	int cnt;
	char *state_str;
	struct dsfs_sb *sbi;
	DSFS_DBG("file=%p, buf=%p, count=%zu, *ppos=%lld",
		file, buf, count, *ppos);
	/* Any read anywhere except zero returns EOF */
	if (*ppos)
		return 0;

	sbi = DSFS_SB(file->f_dentry->d_inode->i_sb);
	BUG_ON(!sbi);

	switch (sbi->si_state) {
	case DSFS_INIT:
		state_str = "init\n";
		break;
	case DSFS_READY:
		state_str = "ready\n";
		break;
	case DSFS_PAUSED:
		state_str = "paused\n";
		break;
	default:
		state_str = "<unknown>\n";
	}
	cnt = strlen(state_str);
	if (copy_to_user(buf, state_str, cnt))
		return -EACCES;

	file->f_pos = *ppos = cnt;
	return cnt;
}

/*
 * Move from init to ready state. After mounting DSFS, the user
 * is expected to mount each data server over the appropriate device
 * directory and then write the string "ready" to the mds control
 * file. This causes DSFS to read and cache all dentry/inode for each
 * data server's mount point. This effectively makes layouts valid
 * since each layout contains dev id's that are indices into the super
 * block's device id table.
 */
static int dsfs_init_2_ready(struct dsfs_sb *sbi)
{
	char dev_id[12];
	struct dentry *d;
	struct dsfs_inode *dc_inode;
	int i;

	DSFS_DBG("");
	d = lookup_one_len(DSFS_DC_DIR, sbi->si_sb->s_root,
			   strlen(DSFS_DC_DIR));
	if (!d || !d->d_inode) {
		printk(KERN_ERR "dsfs: Could not find %s directory\n",
		       DSFS_DC_DIR);
		goto err0;
	}
	sbi->si_dc_root = d;
	dc_inode = DSFS_I(d->d_inode);
	BUG_ON(!dc_inode);

	/* Make sure all devices up to dev_count have device id */
	for (i = 0; i < sbi->si_dev_list->dev_count; i++)
		if (sbi->si_dev_list->dev_list[i].dev_id == -1) {
			printk(KERN_ERR
			       "dsfs: cannot move to ready because device %d "
			       "is not defined.\n", i);
			goto err0;
		}

	for (i = 0; i < sbi->si_dev_list->dev_count; i++) {
		sprintf(dev_id, "%d", i);
		d = lookup_one_len(dev_id, sbi->si_sb->s_root,
				   strlen(dev_id));
		if (!d || !d->d_inode) {
			printk(KERN_ERR
			       "dsfs: Could not find device file %s\n",
			       dev_id);
			goto err1;
		}
		dc_inode->ii_dev_list[i] = dget(d);
		DSFS_DBG("dev_id %d d %p d->d_count %d", i, d, atomic_read(&d->d_count));
	}
	sbi->si_state = DSFS_READY;
	return 0;

 err1:
	/* unput all the dentry */
	for (i = 0; i < sbi->si_dev_list->dev_count; i++)
		if (dc_inode->ii_dev_list[i]) {
			dput(d);
			dc_inode->ii_dev_list[i] = NULL;
		}
 err0:
	return -ENOENT;
}

static int dsfs_ready_2_init(struct dsfs_sb *sbi)
{
	int i;
	struct dsfs_inode *dc_inode;

	if (sbi->si_dc_root) {
		dc_inode = DSFS_I(sbi->si_dc_root->d_inode);
		BUG_ON(!dc_inode);
		for (i = 0; i < sbi->si_dev_list->dev_count; i++)
			if (dc_inode->ii_dev_list[i]) {
				DSFS_DBG("dc_inode->ii_dev_root[%d]->d_count=%d",
					i, atomic_read(&(dc_inode->ii_dev_list[i]->d_count)));
				dput(dc_inode->ii_dev_list[i]);
				dc_inode->ii_dev_list[i] = NULL;
			}

		DSFS_DBG("sbi->si_dc_root->d_count=%d", atomic_read(&sbi->si_dc_root->d_count));
		dput(sbi->si_dc_root);
		sbi->si_dc_root = NULL;
		sbi->si_state = DSFS_INIT;
	}
	return 0;
}

/*
 * Set the filesystem state. For now, only going to ready does anything
 */
int dsfs_set_state(struct dsfs_sb *sbi, int new_state)
{
	int err = 0;
	DSFS_DBG("current_state=%d, new_state=%d", sbi->si_state, new_state);
	switch (sbi->si_state) {
	case DSFS_INIT:
		switch (new_state) {
		case DSFS_INIT:
			break;
		case DSFS_READY:
			err = dsfs_init_2_ready(sbi);
			break;
		case DSFS_PAUSED:
			err = -EINVAL;
			break;
		default:
			BUG();
		}
		break;
	case DSFS_READY:
		switch (new_state) {
		case DSFS_INIT:
			err = dsfs_ready_2_init(sbi);
			break;
		case DSFS_READY:
			break;
		case DSFS_PAUSED:
			sbi->si_state = DSFS_PAUSED;
			break;
		default:
			BUG();
		}
		break;
	case DSFS_PAUSED:
		switch (new_state) {
		case DSFS_INIT:
			sbi->si_state = DSFS_INIT;
			break;
		case DSFS_READY:
			err = -EINVAL;
			break;
		case DSFS_PAUSED:
			break;
		default:
			BUG();
		}
		break;
	default:
		BUG();
	}
 	DSFS_DBG("err=%d", err);
	return err;
}

ssize_t dsfs_mds_write(struct file *file, const char *buf, size_t count,
			loff_t *ppos)
{
	struct dsfs_sb *sbi;
	char state_str[16];
	int err = 0;

	if (*ppos != 0 || count > 16)
		return -EINVAL;

	if (copy_from_user(state_str, buf, count))
		return -EINVAL;

	sbi = DSFS_SB(file->f_dentry->d_inode->i_sb);
	BUG_ON(!sbi);

	if (strncmp(state_str, "init", strlen("init")) == 0)
		err = dsfs_set_state(sbi, DSFS_INIT);
	else if (strncmp(state_str, "ready", strlen("ready")) == 0)
		err = dsfs_set_state(sbi, DSFS_READY);
	else if (strncmp(state_str, "pause", strlen("pause")) == 0)
		err = dsfs_set_state(sbi, DSFS_PAUSED);
	if (!err) {
		*ppos = count;
		err = count;
	}
	DSFS_DBG("err=%d", err);
	return err;
}
