/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/fs.h>
#include <linux/exportfs.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/file.h>
#include <linux/mm_types.h>
#include <linux/smp_lock.h>
#include <linux/uaccess.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_DC_FILE

static loff_t dsfs_dc_llseek_sd(struct dsfs_layout *lo, struct inode *inode,
				struct file *file, loff_t offset)
{
	int stripe_no;
	loff_t stripe_off;
	int dev_id;
	loff_t stripe_pos;
	struct file *dev_file;
	int stripe_cnt;

	stripe_no = offset / lo->stripe_size;
	stripe_off = offset % lo->stripe_size;
	dev_id = stripe_no % lo->dev_count;
	stripe_cnt = stripe_no / lo->dev_count;
	stripe_pos = (stripe_cnt * lo->stripe_size) + stripe_off;
	dev_file = lo->dev_file[dev_id];

	DSFS_DBG("[%d] dev_file %p offset %llu", dev_id, dev_file, stripe_pos);
	stripe_pos = vfs_llseek(dev_file, stripe_pos, SEEK_SET);
	if (stripe_pos < 0)
		printk(KERN_INFO
		       "dsfs: error %llu trying to llseek stripe file for "
		       "device %d\n", stripe_pos, dev_id);

	return stripe_pos;
}

static loff_t dsfs_dc_llseek_ss(struct dsfs_layout *lo, struct inode *inode,
				struct file *file, loff_t offset)
{
	return -EINVAL;
}

static loff_t dsfs_dc_llseek_rr(struct dsfs_layout *lo, struct inode *inode,
				struct file *file, loff_t offset)
{
	return -EINVAL;
}

/**
 * dsfs_dc_llseek - Move the current file position to the specified location
 *
 * @file: Pointer to file structure. This actually refers to the
 * layout file in the 'dc' directory. The current file position is maintained
 * here, but each stripe file is also llseek'd after converting to the stripe
 * relative offset.
 *
 * @offset: Offset to seek.
 *
 * @origin: The offset is either relative to the current file position
 * (SEEK_CUR), end of file (SEEK_END), or beginning of file (SEEK_START)
 **/
static loff_t dsfs_dc_llseek(struct file *file, loff_t offset, int origin)
{
	loff_t retval;
	loff_t stripe_ret;
	struct inode *inode = file->f_mapping->host;
	struct dsfs_layout *lo = DSFS_I(inode)->ii_layout;

	DSFS_DBG("file %p inode %p lo %p offset %llu origin %d",
 		 file, inode, lo, offset, origin);

	mutex_lock(&inode->i_mutex);
	switch (origin) {
	case SEEK_END:
		origin += inode->i_size;
		break;
	case SEEK_CUR:
		origin += file->f_pos;
		break;
	}
	retval = -EINVAL;
	if (offset >= 0 && offset <= inode->i_sb->s_maxbytes) {
		if (offset != file->f_pos) {
			file->f_pos = offset;
			file->f_version = 0;
		}
		retval = offset;
	}
	mutex_unlock(&inode->i_mutex);
	if (retval < 0)
		goto out;

	switch (lo->stripe_type) {
	case DSFS_STRIPE_DENSE:
		stripe_ret = dsfs_dc_llseek_sd(lo, inode, file, offset);
		break;
	case DSFS_STRIPE_SPARSE:
		stripe_ret = dsfs_dc_llseek_ss(lo, inode, file, offset);
		break;
	case DSFS_ROUND_ROBIN:
		stripe_ret = dsfs_dc_llseek_rr(lo, inode, file, offset);
		break;
	}

 out:
	return retval;
}

static ssize_t dsfs_dc_read_sd(struct dsfs_layout *lo,
			       struct file *file, char *buf,
			       size_t count, loff_t *ppos)
{
	int err = 0;
	int stripe_no;
	loff_t stripe_off;
	size_t stripe_rsize;
	loff_t pos = *ppos;
	int dev_id;
	ssize_t bytes = 0;

	DSFS_DBG("file=%p, buf=%p, count=%zu, *ppos=%lld, lo=%p, lo->fsize=%lld",
		file, buf, count, *ppos, lo, lo->fsize);

	if (!count)
		goto out;

	/* Read each of the stripe files */
	stripe_no = pos / lo->stripe_size;
	stripe_off = pos % lo->stripe_size;
	dev_id = stripe_no % lo->dev_count;
	stripe_rsize = lo->stripe_size - stripe_off;
	stripe_rsize = min(stripe_rsize, count);
	while (count && pos < lo->fsize) {
		loff_t spos;
		struct file *dev_file;
		int stripe_cnt;

		if (stripe_rsize + pos > lo->fsize)
			stripe_rsize = lo->fsize - pos;
		stripe_cnt = stripe_no / lo->dev_count;
		spos = (stripe_cnt * lo->stripe_size) + stripe_off;
		dev_file = lo->dev_file[dev_id];
		DSFS_DBG("[%d] dev_file %p buf %p stripe_rsize %zu pos %llu",
			 dev_id, dev_file, buf, stripe_rsize, spos);
		err = dev_file->f_op->read(dev_file, buf, stripe_rsize, &spos);
		if (err < 0) {
			printk(KERN_INFO
			       "dsfs: error %d trying to read stripe file for "
			       "device %d\n", err, dev_id);
			goto out;
		}
		if (err != stripe_rsize) {
			DSFS_DBG("dsfs: truncated stripe file err %d stripe_rsize %zu",
				 err, stripe_rsize);
			goto out;
		}
		dev_id++;
		dev_id %= lo->dev_count;
		stripe_off = 0;
		stripe_no++;
		bytes += stripe_rsize;
		count -= stripe_rsize;
		pos += stripe_rsize;
		buf += stripe_rsize;
		stripe_rsize = min(count, lo->stripe_size);
	}
 out:
	file->f_pos = min_t(loff_t, pos, lo->fsize);
	*ppos = file->f_pos;
	return bytes;
}

static ssize_t dsfs_dc_read_ss(struct dsfs_layout *lo,
			       struct file *file, char *buf,
			       size_t count, loff_t *ppos)
{
	return -EINVAL;
}

static ssize_t dsfs_dc_read_rr(struct dsfs_layout *lo,
			       struct file *file, char *buf,
			       size_t count, loff_t *ppos)
{
	return -EINVAL;
}

/**
 * dsfs_dc_read - Read data from a striped file
 *
 * @file: Pointer to file structure. This actually refers to the
 * layout file in the 'dc' directory. Writes actually go to the device
 * files, not the layout file.
 *
 * @buf: Pointer to buffer to write.
 *
 * @count: Number of bytes tor read.
 *
 * @ppos: Pointer to loff_t that contains the initial file
 * position. Updated with final position. Note that this position is
 * the 'virtual' position in the file, not the actuall position in any
 * of the striped files.
 **/
static ssize_t dsfs_dc_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	struct inode *inode = file->f_dentry->d_inode;
	struct dsfs_layout *lo = DSFS_I(inode)->ii_layout;

	DSFS_DBG("file=%p, buf=%p, count=%zu, *ppos=%lld, lo=%p, lo->fsize=%lld",
		file, buf, count, *ppos, lo, lo->fsize);

	if (*ppos >= lo->fsize)
		return 0;

	switch (lo->stripe_type) {
	case DSFS_STRIPE_DENSE:
		return dsfs_dc_read_sd(lo, file, buf, count, ppos);
	case DSFS_STRIPE_SPARSE:
		return dsfs_dc_read_ss(lo, file, buf, count, ppos);
	case DSFS_ROUND_ROBIN:
		return dsfs_dc_read_rr(lo, file, buf, count, ppos);
	}
 	return -EINVAL;
}

static ssize_t dsfs_dc_write_sd(struct dsfs_layout *lo,
				struct file *filp, const char *buf,
				size_t count, loff_t *ppos)
{
	int err = 0;
	int stripe_no;
	loff_t stripe_off;
	size_t stripe_wsize;
	int dev_id;
	loff_t pos = *ppos;

	DSFS_DBG("file=%p, buf=%p, count=%zu, *ppos=%llu, lo=%p",
		filp, buf, count, *ppos, lo);
	if (!count)
		goto out;

	/* adjust for append -- seek to the end of the file */
	if ((filp->f_flags & O_APPEND))
		pos = lo->fsize;

	/* Write each of the stripe files */
	stripe_no = pos / lo->stripe_size;
	stripe_off = pos % lo->stripe_size;
	dev_id = stripe_no % lo->dev_count;
	stripe_wsize = lo->stripe_size - stripe_off;
	stripe_wsize = min(stripe_wsize, count);
	while (count) {
		loff_t spos;
		struct file *dev_file;
		int stripe_cnt;

		stripe_cnt = stripe_no / lo->dev_count;
		spos = (stripe_cnt * lo->stripe_size) + stripe_off;
		dev_file = lo->dev_file[dev_id];
		DSFS_DBG("[%d] dev_file %p buf %p stripe_wsize %zu pos %llu",
			 dev_id, dev_file, buf, stripe_wsize, spos);
		err = vfs_write(dev_file, buf, stripe_wsize, &spos);
		if (err < 0) {
			printk(KERN_INFO
			       "dsfs: error %d trying to write stripe file for "
			       "device %d\n", err, dev_id);
			goto out;
		}
		dev_id++;
		dev_id %= lo->dev_count;
		stripe_off = 0;
		stripe_no ++;
		count -= stripe_wsize;
		pos += stripe_wsize;
		buf += stripe_wsize;
		stripe_wsize = min(count, lo->stripe_size);
	}
 out:
	filp->f_pos = pos;
	*ppos = pos;
	lo->fsize = max_t(loff_t, lo->fsize, pos);
	i_size_write(filp->f_dentry->d_inode, lo->fsize);
	return err;
}

static ssize_t dsfs_dc_write_ss(struct dsfs_layout *lo,
				struct file *filp, const char *buf,
				size_t count, loff_t *ppos)
{
	return -EINVAL;
}

static ssize_t dsfs_dc_write_rr(struct dsfs_layout *lo,
				struct file *filp, const char *buf,
				size_t count, loff_t *ppos)
{
	return -EINVAL;
}

/**
 * dsfs_dc_write - Write data to a striped file
 *
 * @file: Pointer to file structure. This actually refers to the
 * layout file in the 'dc' directory. Writes actually go to the device
 * files, not the layout file.
 *
 * @buf: Pointer to buffer to write.
 *
 * @ppos: Pointer to loff_t that contains the initial file
 * position. Updated with final position. Note that this position is
 * the 'virtual' position in the file, not the actual position in any
 * of the striped files.
 **/
static ssize_t dsfs_dc_write(struct file *filp,
			     const char *buf, size_t count, loff_t *ppos)
{
	struct inode *inode = filp->f_dentry->d_inode;
	struct dsfs_layout *lo;

	lo = DSFS_I(inode)->ii_layout;
	/* Sanity check the layout */
	if (!lo || !lo->stripe_size || lo->dev_count > DSFS_LAYOUT_MAX_DEVS) {
		printk(KERN_INFO "Invalid layout for file %s\n", 
		       filp->f_dentry->d_name.name);
		goto out;
	}
	DSFS_DBG("file=%p, buf=%p, count=%zu, *ppos=%llu, lo=%p",
		filp, buf, count, *ppos, lo);

	switch (lo->stripe_type) {
	case DSFS_STRIPE_DENSE:
		return dsfs_dc_write_sd(lo, filp, buf, count, ppos);
	case DSFS_STRIPE_SPARSE:
		return dsfs_dc_write_ss(lo, filp, buf, count, ppos);
	case DSFS_ROUND_ROBIN:
		return dsfs_dc_write_rr(lo, filp, buf, count, ppos);
	}
 out:
	return -EINVAL;
}

static int dsfs_dc_readdir(struct file *file, void *dirent, filldir_t filldir)
{
	int err = -ENOTDIR;
	struct file *h_file;
	struct inode *inode;

	DSFS_DBG("file=%p, dirent=%p", file, dirent);
	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);

	inode = file->f_dentry->d_inode;

	h_file->f_pos = file->f_pos;
	err = vfs_readdir(h_file, filldir, dirent);
	file->f_pos = h_file->f_pos;
	if (err >= 0)
		inode->i_atime = h_file->f_dentry->d_inode->i_atime;

	return err;
}

static int dsfs_dc_ioctl(struct inode *inode, struct file *file,
			  unsigned int cmd, unsigned long arg)
{
	int err = -EINVAL;
	struct file *h_file;
	DSFS_DBG("inode=%p, file=%p, cmd=%d, arg=%lu",
		inode, file, cmd, arg);

	h_file = dsfs_h_file(file);
	if (h_file && h_file->f_op && h_file->f_op->ioctl)
		err = h_file->f_op->ioctl(dsfs_h_inode(inode), h_file, cmd, arg);

	return err;
}

static int dsfs_dc_mmap(struct file *file, struct vm_area_struct *vma)
{
	int err = -EINVAL;
	struct file *h_file;

	DSFS_DBG("file=%p, vma=%p", file, vma);

	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);
	if (h_file->f_op && h_file->f_op->mmap) {
		vma->vm_file = h_file;
		err = h_file->f_op->mmap(h_file, vma);
		get_file(h_file);
		fput(file);
	}
	return err;
}


/*
 *
 */
static int dsfs_dc_open(struct inode *inode, struct file *file)
{
	struct file *h_file;
	struct dentry *h_dentry;
	struct vfsmount *h_mnt;
	struct dsfs_inode *dc_inode = DSFS_I(inode);
	struct dsfs_inode *dc_dir;
	struct dentry *parent;
	int i;

	DSFS_DBG("inode=%p, file=%p, name=%s", inode, file,
		file->f_dentry->d_name.name);
	file->private_data = kzalloc(sizeof(struct dsfs_file),
				     GFP_KERNEL);
	if (!file->private_data)
		return -ENOMEM;

	h_dentry = dsfs_h_dentry(file->f_dentry);
	dget(h_dentry);

	/*
	 * dentry_open will decrement mnt refcnt if err.
	 * otherwise fput() will do an mntput() for us upon file close.
	 */
	h_mnt = dsfs_h_mnt(file->f_dentry);
	BUG_ON(!h_mnt);
	mntget(h_mnt);
	h_file = dentry_open(h_dentry, h_mnt, file->f_flags);
	if (IS_ERR(h_file))
		goto err_out;

	DSFS_F(file)->fi_h_file = h_file;
	if (!S_ISREG(h_dentry->d_inode->i_mode))
		goto out;

	/* Open the stripe files */
	parent = dget(file->f_dentry->d_parent);
	dc_dir = DSFS_I(parent->d_inode);
	DSFS_DBG("dc_inode->ii_layout = %p", dc_inode->ii_layout);
	BUG_ON(!dc_inode->ii_layout);
	for (i = 0; i < dc_inode->ii_layout->dev_count; i++) {
		int dev_id;
		struct dentry *dev_parent;
		struct dentry *dev_stripe;
		dev_id = dc_inode->ii_layout->dev_list[i];
		dev_parent = dc_dir->ii_dev_list[dev_id];
		if (!dev_parent) {
			printk(KERN_INFO
			       "dsfs: Device %d is not initialized.\n", dev_id);
			continue;
		}
		DSFS_DBG("i=%d, dev_id=%d, d=%p", i, dev_id, dev_parent);
		dev_stripe = lookup_one_len(file->f_dentry->d_name.name, dev_parent,
					    file->f_dentry->d_name.len);
		DSFS_DBG("dc=%p", dev_stripe);
		if (IS_ERR(dev_stripe)) {
			printk(KERN_INFO "dsfs: Error looking up stripe file %s\n",
			       file->f_dentry->d_name.name);
			continue;
		}
		if (!dev_stripe->d_inode) {
			printk(KERN_INFO
			       "dsfs: stripe file %s on device %d "
			       "does not exit\n", file->f_dentry->d_name.name, dev_id);
			goto next;
		}
		h_dentry = dsfs_h_dentry(dev_stripe);
		DSFS_DBG("dev_stripe=%p, h_dentry=%p", dev_stripe, h_dentry);
		dc_inode->ii_layout->dev_file[i] =
			dentry_open(dget(h_dentry), mntget(h_mnt),
				    file->f_flags);
		DSFS_DBG("filp=%p", dc_inode->ii_layout->dev_file[i]);
	next:			     
		DSFS_DBG("dev_stripe->d_count=%d", atomic_read(&dev_stripe->d_count));
		dput(dev_stripe);
	}
 out:
	return 0;

err_out:
	kfree(file->private_data);
	return PTR_ERR(h_file);
}


static int dsfs_dc_flush(struct file *file, fl_owner_t id)
{
	int err = 0;
	struct file *lo_file;
	struct dentry *lo_dentry;
	struct vfsmount *lo_mnt;
	struct file *dev_file;
	struct inode *inode = file->f_dentry->d_inode;
	struct dsfs_layout *lo;
	int dev_id;

	DSFS_DBG("file %p", file);

	lo = DSFS_I(inode)->ii_layout;
	if (!lo || lo->dev_count > DSFS_LAYOUT_MAX_DEVS) {
		printk(KERN_INFO "Invalid layout for file %s\n", 
		       file->f_dentry->d_name.name);
		goto out;
	}

	for (dev_id = 0; dev_id < lo->dev_count; dev_id++) {
		dev_file = lo->dev_file[dev_id];
		DSFS_DBG("flushing %d h_file %p", dev_id, dev_file);
		if (dev_file && dev_file->f_op && dev_file->f_op->flush)
			err = dev_file->f_op->flush(dev_file, id);
	}

	/* Update the layout file */
	lo_dentry = dsfs_h_dentry(file->f_dentry);
	lo_mnt = dsfs_h_mnt(file->f_dentry);
	lo_file = dentry_open(dget(lo_dentry), mntget(lo_mnt), O_RDWR);
	dsfs_write_layout_kern(lo_file, lo);
	i_size_write(file->f_dentry->d_inode, lo->fsize);
	filp_close(lo_file, current->files);
	DSFS_DBG("size %llu count %lu",
		 i_size_read(file->f_dentry->d_inode),
		 file->f_dentry->d_inode->i_blocks);
 out:
	return err;
}

static int dsfs_dc_release(struct inode *inode, struct file *file)
{
	struct dsfs_inode *dc_inode = DSFS_I(inode);
	int i;
	DSFS_DBG("inode=%p, file=%p", inode, file);

	if (!S_ISREG(inode->i_mode))
		goto out;

	BUG_ON(!dc_inode->ii_layout);
	for (i = 0; i < dc_inode->ii_layout->dev_count; i++) {
		struct file *devf = dc_inode->ii_layout->dev_file[i];
		if (devf && !IS_ERR(devf))
			filp_close(devf, current->files);
	}
			
 out:
	fput(dsfs_h_file(file));
	kfree(file->private_data);
	return 0;
}


static int dsfs_dc_fsync(struct file *file, struct dentry *dentry, int datasync)
{
	int err = -EINVAL;
	struct file *h_file;
	struct dentry *h_dentry = dsfs_h_dentry(dentry);

	DSFS_DBG("file=%p, dentry=%p, datasync=%d", file, dentry, datasync);

	/*
	 * When exporting upper file system through NFS with the sync
	 * option, nfsd_sync_dir() sets struct file as NULL. Use
	 * inode's i_fop->fsync instead of file's.  see fs/nfsd/vfs.c
	 */
	if (file == NULL) {
		if (h_dentry->d_inode->i_fop && h_dentry->d_inode->i_fop->fsync) {
			mutex_lock(&h_dentry->d_inode->i_mutex);
			err = h_dentry->d_inode->i_fop->fsync(NULL, h_dentry, datasync);
			mutex_unlock(&h_dentry->d_inode->i_mutex);
		}
	} else {
		h_file = dsfs_h_file(file);
		if (h_file && h_file->f_op && h_file->f_op->fsync) {
			mutex_lock(&h_dentry->d_inode->i_mutex);
			err = h_file->f_op->fsync(h_file, h_dentry, datasync);
			mutex_unlock(&h_dentry->d_inode->i_mutex);
		}
	}
	return err;
}

static int dsfs_dc_fasync(int fd, struct file *file, int flag)
{
	int err = 0;
	struct file *h_file = dsfs_h_file(file);

	DSFS_DBG("fd=%d, file=%p, flag=%d", fd, file, flag);
	BUG_ON(!h_file);

	if (h_file->f_op && h_file->f_op->fasync)
		err = h_file->f_op->fasync(fd, h_file, flag);

	return err;
}

#if 0
static inline void __locks_delete_block(struct file_lock *waiter)
{
	list_del_init(&waiter->fl_block);
	list_del_init(&waiter->fl_link);
	waiter->fl_next = NULL;
}

static void locks_delete_block(struct file_lock *waiter)
{
	lock_kernel();
	__locks_delete_block(waiter);
	unlock_kernel();
}

static inline int dsfs_posix_lock(struct file *file, struct file_lock *fl, int cmd)
{
    int error;
    for(;;) {
	error = posix_lock_file(file, fl);
	if ((error != -EAGAIN) || (cmd == F_SETLK))
	    break;

	error = wait_event_interruptible(fl->fl_wait, !fl->fl_next);
	if(!error)
	    continue;

	locks_delete_block(fl);
	break;
    }

    return error;
}

static int dsfs_setlk(struct file *file, int cmd, struct file_lock *fl)
{
	int err;
	struct inode *inode, *h_inode;
	struct file *h_file = NULL;

	DSFS_DBG("file=%p, cmd=%d, fl=%p", file, cmd, fl);

	error = -EINVAL;
	BUG_ON(!STRUCT FILEO_PRIVATE(file));
	h_file = STRUCT FILEO_LOWER(file);
	BUG_ON(!h_file);

	inode = file->f_dentry->d_inode;
	h_inode = h_file->f_dentry->d_inode;

	/* Don't allow mandatory locks on files that may be memory mapped
	 * and shared.
	 */
	if (IS_MANDLOCK(h_inode) &&
	    (h_inode->i_mode & (S_ISGID | S_IXGRP)) == S_ISGID &&
	    mapping_writably_mapped(h_file->f_mapping)) {
		error = -EAGAIN;
		goto out;
	}

	if (cmd == F_SETLKW) {
		fl->fl_flags |= FL_SLEEP;
	}

	error = -EBADF;
	switch (fl->fl_type) {
	case F_RDLCK:
		if (!(h_file->f_mode & FMODE_READ))
			goto out;
		break;
	case F_WRLCK:
		if (!(h_file->f_mode & FMODE_WRITE))
			goto out;
		break;
	case F_UNLCK:
		break;
	default:
		error = -EINVAL;
		goto out;
	}

	fl->fl_file = h_file;
	error = security_file_lock(h_file, fl->fl_type);
	if (error)
		goto out;

	if (h_file->f_op && h_file->f_op->lock != NULL) {
		error = h_file->f_op->lock(h_file, cmd, fl);
		if (error)
			goto out;
		goto upper_lock;
	}

	error = dsfs_posix_lock(h_file, fl, cmd);
	if (error)
		goto out;


 upper_lock:
	fl->fl_file = file;
	error = dsfs_posix_lock(file, fl, cmd);
	if (error) {
		fl->fl_type = F_UNLCK;
		fl->fl_file = h_file;
		dsfs_posix_lock(h_file, fl, cmd);
	}

 out:
	print_exit_status(error);
	return error;
}

static int dsfs_getlk(struct file *file, struct file_lock *fl)
{
	int err;
	struct file_lock *tempfl = NULL;c
	struct file_lock cfl;

	DSFS_DBG("file=%p, fl=%p", file, fl);

	if (file->f_op && file->f_op->lock) {
		err = file->f_op->lock(file, F_GETLK, fl);
		if (err < 0)
			goto out;
	} else
		tempfl = (posix_test_lock(file, fl, &cfl) ? &cfl : NULL);

	if (!tempfl)
		fl->fl_type = F_UNLCK;
	else
		memcpy(fl, tempfl, sizeof(struct file_lock));

 out:
	return error;
}

static int dsfs_lock(struct file *file, int cmd, struct file_lock *fl)
{
	int err = -EINVAL;
	struct file *h_file = dsfs_h_file(file);
	struct file_lock *tmpfl = NULL;

	DSFS_DBG("file=%p, cmd=%d, fl=%p", file, cmd, fl);
	BUG_ON(!h_file);

	if (!fl)
		return err;

	fl->fl_file = h_file;
	switch(cmd) {
		case F_GETLK:
		case F_GETLK64:
			err = dsfs_getlk(h_file, fl);
			break;

		case F_SETLK:
		case F_SETLKW:
		case F_SETLK64:
		case F_SETLKW64:
			fl->fl_file = file;
			err = dsfs_setlk(file, cmd, fl);
			break;

	}
	fl->fl_file = file;
	return err;
}

static ssize_t dsfs_dc_sendfile(struct file *file, loff_t *ppos,
			      size_t count, read_actor_t actor, void *target)
{
	struct file *h_file = dsfs_h_file(file);
	int err = -EINVAL;

	BUG_ON(!h_file);
	if (h_file->f_op && h_file->f_op->sendfile)
		err = h_file->f_op->sendfile(h_file, ppos, count,
						actor, target);

	return err;
}
#endif

struct file_operations dsfs_dc_fops =
{
	.read =      dsfs_dc_read,
	.write =     dsfs_dc_write,
	.llseek =    dsfs_dc_llseek,
	.readdir =   dsfs_dc_readdir,
	.ioctl =     dsfs_dc_ioctl,
	.mmap =      dsfs_dc_mmap,
	.open =      dsfs_dc_open,
	.flush =     dsfs_dc_flush,
	.release =   dsfs_dc_release,
	.fsync =     dsfs_dc_fsync,
	.fasync =    dsfs_dc_fasync,
#if 0
	.lock =      dsfs_lock,
	.sendfile =  dsfs_sendfile,
#endif
};

