/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/pagemap.h>
#include <linux/highmem.h>
#include <linux/time.h>
#include <linux/init.h>
#include <linux/string.h>
#include <linux/backing-dev.h>
#include <linux/sched.h>
#include <linux/mount.h>
#include <linux/vmalloc.h>
#include <asm/uaccess.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_INODE

/*
 * dsfs_iget - Allocate and initialize a dsfs inode.
 */
struct inode *dsfs_iget(struct super_block *sb, unsigned long ino)
{
	static struct address_space_operations __empty_aops;
	struct inode *inode;

	DSFS_DBG("sb=%p, ino=%ld", sb, ino);
	inode = iget_locked(sb, ino);
	if (!inode)
		return ERR_PTR(-ENOMEM);

	if (!(inode->i_state & I_NEW))
		return inode;

	inode->i_version++;
	inode->i_op = &dsfs_root_iops;
	inode->i_fop = &dsfs_root_fops;
	inode->i_mapping->a_ops = &__empty_aops;
	DSFS_I(inode)->ii_h_inode = NULL;
	unlock_new_inode(inode);

	return inode;
}

/**
 * dsfs_inode_attach - attach a DSFS inode to a host FS inode.
 *
 * @h_dentry - host fs dentry
 * @dentry - my dentry for new file
 * @sb - my sb
 * @ppi - a pointer to store new inode. May be null.
 *
 * Create a DSFS inode that is based on the host dentry provided by
 * h_dentry.
 *
 **/
int dsfs_inode_attach(struct dentry *h_dentry, struct dentry *dentry,
		       struct super_block *sb, struct inode **ppi)
{
	struct inode *h_inode;
	struct inode *inode;

	DSFS_DBG("h_dentry=%p, dentry=%p, sb=%p, ppi=%p",
		h_dentry, dentry, sb, ppi);

	h_inode = h_dentry->d_inode;
	BUG_ON(!h_inode);

	/* Check that h_dentry didn't cross a mount point */
	if (h_inode->i_sb != DSFS_SB(sb)->si_h_sb) {
		DSFS_DBG("crossed mount point!");
		return -EXDEV;
	}

	DSFS_DBG("sb->s_op=%p, h_inode->i_ino=%lu", sb->s_op, h_inode->i_ino);
	inode = dsfs_iget(sb, h_inode->i_ino);
	if (!inode || IS_ERR(inode))
		return -ENOMEM;

	/* Attach the host inode to our inode */
	DSFS_DBG("d_inode->h_inode = %p", DSFS_I(inode)->ii_h_inode);
	DSFS_I(inode)->ii_h_inode = igrab(h_inode);

	/* Setup our inode ops */
	DSFS_DBG("h_inode->i_mode=%08x", h_inode->i_mode);
	if (S_ISLNK(h_inode->i_mode)) {
		DSFS_DBG("init link");
		inode->i_op = &dsfs_symlink_iops;
	} else if (S_ISDIR(h_inode->i_mode)) {
		DSFS_DBG("init directory");
		inode->i_op = &dsfs_dir_iops;
 		inode->i_fop = &dsfs_dir_fops;
	} else if (S_ISCHR(h_inode->i_mode) ||
		   S_ISBLK(h_inode->i_mode) ||
		   S_ISFIFO(h_inode->i_mode) ||
		   S_ISSOCK(h_inode->i_mode)) {
		DSFS_DBG("init special");
		init_special_inode(inode, h_inode->i_mode, h_inode->i_rdev);
	}

	inode->i_mapping->a_ops = h_inode->i_mapping->a_ops;
	DSFS_DBG("dentry=%p, inode=%p", dentry, inode);
	if (dentry)
		d_instantiate(dentry, inode);
	DSFS_DBG("inode=%p, h_inode=%p", inode, h_inode);
	dsfs_copy_attr_all(inode, h_inode);

	if (ppi)
		*ppi = inode;

	return 0;
}

/*
 * dsfs_dentry_map - Attach a DSFS dentry to a host dentry
 */
int dsfs_dentry_attach(struct dentry *h_dentry, struct dentry *dentry)
{
	dentry->d_op = &dsfs_dops;

	DSFS_DBG("");

	/* link the DSFS and host dentries */
	dentry->d_fsdata = kzalloc(sizeof(struct dsfs_dentry), GFP_KERNEL);
	if (!dentry->d_fsdata)
		return -ENOMEM;

	DSFS_D(dentry)->di_h_dentry = dget(h_dentry);
	DSFS_D(dentry)->di_h_mnt =
		mntget(DSFS_D(dentry->d_sb->s_root)->di_h_mnt);
	return 0;
}

/*
 * dsfs_create - create a regular file
 *
 * @dir : The directory inode to contain the file
 * @dentry : The dentry for the new file
 * @mode : file persmissions
 * @nd : nameidate structure for the new file
 */
int dsfs_create(struct inode *dir, struct dentry *dentry, int mode,
		 struct nameidata *nd)
{
	int err;
	struct dentry *h_dentry;
	struct vfsmount *h_mount;
	struct dentry *h_dir_dentry;
	struct dsfs_nd_args nd_;
	struct dsfs_inode *inode;

	DSFS_DBG("dir=%p, dentry=%p, mode=%08x, name=%.*s",
		dir, dentry, mode,
		(nd?nd->path.dentry->d_name.len:10),
		(nd?(char *)nd->path.dentry->d_name.name:"<nul>"));

	h_dentry = dsfs_h_dentry(dentry);
	DSFS_DBG("h_dentry->d_count=%d", atomic_read(&h_dentry->d_count));
	h_mount = dsfs_h_mnt(dentry);

	/* get the host fs dir dentry */
	h_dir_dentry = dget(h_dentry->d_parent);
	BUG_ON(!h_dir_dentry);
	DSFS_DBG("dir=%p, h_dir_dentry->d_inode=%p", 
		dir, h_dir_dentry->d_inode);
		
	mutex_lock(&h_dir_dentry->d_inode->i_mutex);

	dsfs_nd_push_(&nd_, nd, h_dentry, h_mount);
	err = vfs_create(h_dir_dentry->d_inode, h_dentry, mode, nd);
	dsfs_nd_pop(&nd_, nd);
	if (err)
		goto out;

	err = dsfs_inode_attach(h_dentry, dentry, dir->i_sb, NULL);
	if (err)
		goto out;

	inode = DSFS_I(dentry->d_inode);
	BUG_ON(!inode);

	DSFS_DBG("setting inode type to %d", DSFS_I(dir)->ii_type);
	/* inodes inherit type of parent */
	inode->ii_type = DSFS_I(dir)->ii_type;

	dsfs_copy_attr_timesizes(dir, h_dir_dentry->d_inode);

	DSFS_DBG("DSFS local create: file %s ino %lu",
		dentry->d_name.name, dentry->d_inode->i_ino);
 out:
	unlock_dir(h_dir_dentry);
	DSFS_DBG("err=%d", err);
	return err;
}

/*
 * dsfs_lookup_internal
 *
 * Interposes on both lower inode and lower dentry.
 */
struct dentry *
dsfs_lookup_internal(struct dentry *h_dentry, struct dentry *dentry,
		      struct super_block *sb)
{
	int err;
	struct inode *inode = NULL;
	struct dentry *new_dentry = NULL;
	struct dentry *anon_dentry = NULL;
	struct dentry *ret_dentry = NULL;

	/*
	 * If lower inode exists, get our inode and interpose.
	 * Inode may not exist because lookup can get negative
	 * dentries that do not have an inode.
	 */
	if (h_dentry->d_inode) {
		err = dsfs_inode_attach(h_dentry, NULL, sb, &inode);
		if (err)
			goto err_out;
	}

	/*
	 * For NFS export, lookup needs to use d_splice_alias rather
	 * than d_add to support reconnecting an anonymous dentry.
	 */
	if (!dentry) {
		/*
		 * Allocate an anonymous dentry.  Returned to decode_fh
		 * to instantiate the top of the disconnected path.
		 */
		ret_dentry = new_dentry = anon_dentry = d_alloc_anon(inode);
		if (!ret_dentry) {
			err = -ENOMEM;
			goto err_out;
		}
	} else {
		/*
		 * Actions/behavior when d_splice_alias returns:
		 *
		 * NULL:
		 * - d_splice_alias did a d_add of original dentry
		 * - we interpose on original dentry
		 * - we return NULL from lookup
		 * - caller uses original dentry (that it allocated)
		 *
		 * non-NULL:
		 * - d_splice_alias did a d_move (splice) of anon dentry
		 * - we interpose on this anon dentry
		 * - we return this anon dentry
		 * - caller dput's it's original dentry
		 *
		 * For errors subsequent to this, the caller still handles
		 * cleanup of the dentry (and hopefully all use counts!).
		 */
		ret_dentry = d_splice_alias(inode, dentry);
		if (ret_dentry)
			new_dentry = ret_dentry;
		else
			new_dentry = dentry;
	}

	/* Interpose new dentry on lower dentry. */
	err = dsfs_dentry_attach(h_dentry, new_dentry);
	if (err)
		goto err_out;

	return ret_dentry;

err_out:
	if (anon_dentry)
		dput(anon_dentry);
	if (inode)
		iput(inode);
	return ERR_PTR(err);
}

/*
 * dsfs_lookup
 *
 * &dir: inode of parent directory
 * &dentry: dentry to use (d_alloc'd with d_parent set)
 */
struct dentry *dsfs_lookup(struct inode *dir, struct dentry *dentry,
			    struct nameidata* nd)
{
	int err = 0;
	struct dentry *h_dir_dentry;
	struct dentry *h_dentry;
	struct inode *inode;
	struct vfsmount *h_mnt;

	DSFS_DBG("name=%s", dentry->d_name.name);

	/* Initialize our dentry ops */
	dentry->d_op = &dsfs_dops;
	h_dir_dentry = DSFS_D(dentry->d_parent)->di_h_dentry;

	/* Lookup host FS dentry using it's parent directory. */
	mutex_lock(&h_dir_dentry->d_inode->i_mutex);
	h_dentry = lookup_one_len(dentry->d_name.name, h_dir_dentry,
				  dentry->d_name.len);
	mutex_unlock(&h_dir_dentry->d_inode->i_mutex);
	if (IS_ERR(h_dentry))
		return h_dentry;

	/* Update parent directory's atime */
	dir->i_atime = h_dir_dentry->d_inode->i_atime;

	/* Alloc private dentry */
	dentry->d_fsdata = kzalloc(sizeof(struct dsfs_dentry), GFP_KERNEL);
	if (!dentry->d_fsdata) {
		err = -ENOMEM;
		goto err1;
	}
	/* Take a ref on the mnt */
	h_mnt = mntget(dsfs_h_mnt(dentry->d_parent));

	/*
	 * Initialize private dentry. We have a ref on the dentry and
	 * on the mount
	 */
	DSFS_D(dentry)->di_h_dentry = h_dentry;
	DSFS_D(dentry)->di_h_mnt = h_mnt;

	inode = h_dentry->d_inode;
	if (!inode)
		/* Add negative dentry (i.e. not found) */
		goto out;

	err = dsfs_inode_attach(h_dentry, NULL, dir->i_sb, &inode);
	if (err)
		goto err2;

 out:
	return d_splice_alias(inode, dentry);

 err2:
	mntput(h_mnt);
 err1:
	kfree(DSFS_D(dentry));
	dentry->d_fsdata = NULL;
	d_drop(dentry);
	return ERR_PTR(err);
}

static int
dsfs_link(struct dentry *old_dentry, struct inode *dir, struct dentry *new_dentry)
{
	int err;
	struct dentry *h_old_dentry;
	struct dentry *h_new_dentry;
	struct dentry *h_dir_dentry;
	DSFS_DBG("");
	h_old_dentry = DSFS_D(old_dentry)->di_h_dentry;
	h_new_dentry = DSFS_D(new_dentry)->di_h_dentry;

	dget(h_old_dentry);
	dget(h_new_dentry);
	h_dir_dentry = dsfs_lock_parent(h_new_dentry);


	err = vfs_link(h_old_dentry,
		       h_dir_dentry->d_inode,
		       h_new_dentry);
	if (err || !h_new_dentry->d_inode)
		goto out;

	err = dsfs_inode_attach(h_new_dentry, new_dentry, dir->i_sb, NULL);
	if (err)
		goto out;

	dsfs_copy_attr_timesizes(dir, h_dir_dentry->d_inode);
	/* propagate number of hard-links */
	old_dentry->d_inode->i_nlink = dsfs_h_inode(old_dentry->d_inode)->i_nlink;

 out:
	unlock_dir(h_dir_dentry);
	dput(h_new_dentry);
	dput(h_old_dentry);
	if (!new_dentry->d_inode)
		d_drop(new_dentry);

	return err;
}

static int dsfs_ctl_unlink(struct inode *dir, struct dentry *dentry)
{
	return -EINVAL;
}

static int dsfs_unlink(struct inode *dir, struct dentry *dentry)
{
	int err = 0;
	struct inode *h_dir;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;

	DSFS_DBG("");

	h_dir = dsfs_h_inode(dir);
	h_dentry = dsfs_h_dentry(dentry);

	BUG_ON(!h_dentry);

	dget(dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);

	dget(h_dentry);
	err = vfs_unlink(h_dir, h_dentry);
	dput(h_dentry);
	if (!err)
		d_delete(h_dentry);

	dsfs_copy_attr_times(dir, h_dir);
	dentry->d_inode->i_nlink = dsfs_h_inode(dentry->d_inode)->i_nlink;
	dentry->d_inode->i_ctime = dir->i_ctime;

	unlock_dir(h_dir_dentry);

	if (!err)
		d_drop(dentry);

	dput(dentry);
	return err;
}

static int
dsfs_symlink(struct inode *dir, struct dentry *dentry, const char *symname)
{
	int err;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;
	umode_t	mode;

	h_dentry = dsfs_h_dentry(dentry);

	dget(h_dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);

	mode = S_IALLUGO;

	err = vfs_symlink(h_dir_dentry->d_inode, h_dentry, symname, mode);

	if (err || !h_dentry->d_inode)
		goto out_lock;
	err = dsfs_inode_attach(h_dentry, dentry, dir->i_sb, NULL);
	if (err)
		goto out_lock;

	dsfs_copy_attr_timesizes(dir, h_dir_dentry->d_inode);

out_lock:
	unlock_dir(h_dir_dentry);
	dput(h_dentry);
	if (!dentry->d_inode)
		d_drop(dentry);

	DSFS_DBG("err = %d", err);
	return err;
}

int dsfs_mkdir(struct inode *dir, struct dentry *dentry, int mode)
{
	int err;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;

	DSFS_DBG("");

	h_dentry = dsfs_h_dentry(dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);

	err = vfs_mkdir(h_dir_dentry->d_inode, h_dentry, mode);
	if (err || !h_dentry->d_inode)
		goto out;

	err = dsfs_inode_attach(h_dentry, dentry, dir->i_sb, NULL);
	if (err)
		goto out;

	dsfs_copy_attr_timesizes(dir, h_dir_dentry->d_inode);
	/* update number of links on parent directory */
	dir->i_nlink = h_dir_dentry->d_inode->i_nlink;

out:
	unlock_dir(h_dir_dentry);
	if (!dentry->d_inode)
		d_drop(dentry);

	DSFS_DBG("err = %d", err);
	return err;
}

static int dsfs_rmdir(struct inode *dir, struct dentry *dentry)
{
	int err = 0;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;

	DSFS_DBG("");
	h_dentry = dsfs_h_dentry(dentry);

	dget(dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);

	/* avoid destroying the lower inode if the file is in use */
	dget(h_dentry);
	err = vfs_rmdir(h_dir_dentry->d_inode, h_dentry);
	dput(h_dentry);

	if (!err)			  /* vfs_rmdir does that */
		d_delete(h_dentry);

	dsfs_copy_attr_times(dir, h_dir_dentry->d_inode);
	/* copy the nlink count for our dentry and our parent's dentry */
	dir->i_nlink =  h_dir_dentry->d_inode->i_nlink;

	unlock_dir(h_dir_dentry);

	if (!err)
		d_drop(dentry);

	dput(dentry);

	DSFS_DBG("err = %d", err);
	return err;
}

int dsfs_mknod(struct inode *dir, struct dentry *dentry, int mode, dev_t dev)
{
	int err;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;

	DSFS_DBG("");
	h_dentry = dsfs_h_dentry(dentry);

	h_dir_dentry = dsfs_lock_parent(h_dentry);

	err = vfs_mknod(h_dir_dentry->d_inode,
			h_dentry,
			mode,
			dev);
	if (err || !h_dentry->d_inode)
		goto out;

	err = dsfs_inode_attach(h_dentry, dentry, dir->i_sb, NULL);
	if (err)
		goto out;
	dsfs_copy_attr_timesizes(dir, h_dir_dentry->d_inode);

out:
	unlock_dir(h_dir_dentry);
	if (!dentry->d_inode)
		d_drop(dentry);

	DSFS_DBG("err = %d", err);
	return err;
}

static int
dsfs_rename(struct inode *old_dir, struct dentry *old_dentry,
	      struct inode *new_dir, struct dentry *new_dentry)
{
	int err;
	struct dentry *h_old_dentry;
	struct dentry *h_new_dentry;
	struct dentry *h_old_dir_dentry;
	struct dentry *h_new_dir_dentry;

	DSFS_DBG("");

	h_old_dentry = dsfs_h_dentry(old_dentry);
	h_new_dentry = dsfs_h_dentry(new_dentry);

	dget(h_old_dentry);
	dget(h_new_dentry);
	h_old_dir_dentry = dget_parent(h_old_dentry);
	h_new_dir_dentry = dget_parent(h_new_dentry);
	lock_rename(h_old_dir_dentry, h_new_dir_dentry);

	err = vfs_rename(h_old_dir_dentry->d_inode, h_old_dentry,
			 h_new_dir_dentry->d_inode, h_new_dentry);
	if (!err) {
		dsfs_copy_attr_all(new_dir, h_new_dir_dentry->d_inode);
		if (new_dir != old_dir)
			dsfs_copy_attr_all(old_dir, h_old_dir_dentry->d_inode);
	}
	dput(h_new_dentry);
	dput(h_old_dentry);
	unlock_rename(h_old_dir_dentry, h_new_dir_dentry);

	DSFS_DBG("err = %d", err);
	return err;
}

static int
dsfs_readlink(struct dentry *dentry, char *buf, int bufsiz)
{
	int err;
	struct dentry *h_dentry;

	DSFS_DBG("");
	h_dentry = dsfs_h_dentry(dentry);

	if (!h_dentry->d_inode->i_op ||
	     !h_dentry->d_inode->i_op->readlink) {
		err = -EINVAL;
		goto out;
	}

	err = h_dentry->d_inode->i_op->readlink(h_dentry,
						    buf,
						    bufsiz);
	if (err > 0)
		dentry->d_inode->i_atime = h_dentry->d_inode->i_atime;

out:
	DSFS_DBG("err = %d", err);
	return err;
}

static void *
dsfs_follow_link(struct dentry *dentry, struct nameidata *nd)
{
	char *buf;
	int len = PAGE_SIZE, err;
	mm_segment_t old_fs;

	DSFS_DBG("dentry=%p, name=%.*s",
		dentry, dentry->d_name.len, dentry->d_name.name);

	/* buf is allocated here, and freed when VFS calls our put_link method */
	err = -ENOMEM;
	buf = kmalloc(len, GFP_KERNEL);
	if (!buf)
		goto out;

	old_fs = get_fs();
	set_fs(KERNEL_DS);
	err = dentry->d_inode->i_op->readlink(dentry, buf, len);
	set_fs(old_fs);
	if (err < 0)
		goto out_free;

	buf[err] = 0;	// terminate the buffer -- XXX still needed?
	err = 0;
	nd_set_link(nd,buf);
	goto out;

out_free:
	kfree(buf);
out:
#if 0
	if (err < 0) {
		dput(nd->dentry);
		printk("EZK follow_link() mnt_cnt %d\n", atomic_read(&nd->mnt->mnt_count));
		mntput(nd->mnt);
	}
#endif

	DSFS_DBG("err = %d", err);
	return ERR_PTR(err);
}

void dsfs_put_link(struct dentry *dentry, struct nameidata *nd, void* unused)
{
	kfree(nd_get_link(nd));
}

static int dsfs_permission(struct inode *inode, int mask, struct nameidata* nd)
{
	int err;
	struct inode *h_inode;
	struct dsfs_nd_args nd_;

	h_inode = dsfs_h_inode(inode);
	BUG_ON(!dsfs_h_inode(inode));
	
	dsfs_nd_push(&nd_, nd);
	err = permission(h_inode, mask, nd);
	dsfs_nd_pop(&nd_, nd);

	return err;
}

static int
dsfs_setattr(struct dentry *dentry, struct iattr *ia)
{
	int err = 0;
	struct dentry *h_dentry;
	struct inode *inode;
	struct inode *h_inode;

	DSFS_DBG("");
	h_dentry = dsfs_h_dentry(dentry);
	inode = dentry->d_inode;
	h_inode = dsfs_h_inode(inode);

	err = notify_change(h_dentry, ia);

	/*
	 * The lower file system might have changed the attributes, even if
	 * notify_change above resulted in an error(!)  so we copy the
	 * h_inode's attributes (and a few more) to our inode.
	 */
	dsfs_copy_attr_all(inode, h_inode);

	DSFS_DBG("err = %d", err);
	return err;
}

static ssize_t
dsfs_getxattr(struct dentry *dentry, const char *name, void *value, size_t size)
{
	struct dentry *h_dentry = NULL;
	int err = -ENOTSUPP;

	DSFS_DBG("name=%s, value=%p, size=%zu", name, value, size);

	h_dentry = dsfs_h_dentry(dentry);

	BUG_ON(!h_dentry);
	BUG_ON(!h_dentry->d_inode);
	BUG_ON(!h_dentry->d_inode->i_op);

	DSFS_DBG("getxattr: name=\"%s\", value %lu bytes", name, size);

	if (h_dentry->d_inode->i_op->getxattr) {
		mutex_lock(&h_dentry->d_inode->i_mutex);
		err = h_dentry->d_inode->i_op->getxattr(h_dentry, name, value, size);
		mutex_unlock(&h_dentry->d_inode->i_mutex);
	}

	DSFS_DBG("err = %d", err);
	return err;
}

static int
dsfs_setxattr(struct dentry *dentry, const char *name, const void *value,size_t size, int flags)

{
	struct dentry *h_dentry = NULL;
	int err = -ENOTSUPP;

	DSFS_DBG("name=\"%s\", value=%p, size=%zu, flags=%x", name, value, size, flags);

	h_dentry = dsfs_h_dentry(dentry);

	BUG_ON(!h_dentry);
	BUG_ON(!h_dentry->d_inode);
	BUG_ON(!h_dentry->d_inode->i_op);

	if (h_dentry->d_inode->i_op->setxattr) {
		mutex_lock(&h_dentry->d_inode->i_mutex);
		err = h_dentry->d_inode->i_op->setxattr(h_dentry, name, value, size, flags);
		mutex_unlock(&h_dentry->d_inode->i_mutex);
	}

	DSFS_DBG("err = %d", err);
	return err;
}

static int
dsfs_removexattr(struct dentry *dentry, const char *name)
{
	struct dentry *h_dentry = NULL;
	int err = -ENOTSUPP;
	DSFS_DBG("name = \"%s\"", name);

	h_dentry = dsfs_h_dentry(dentry);

	BUG_ON(!h_dentry);
	BUG_ON(!h_dentry->d_inode);
	BUG_ON(!h_dentry->d_inode->i_op);

	if (h_dentry->d_inode->i_op->removexattr) {
		mutex_lock(&h_dentry->d_inode->i_mutex);
		err = h_dentry->d_inode->i_op->removexattr(h_dentry, name);
		mutex_unlock(&h_dentry->d_inode->i_mutex);
	}

	DSFS_DBG("err = %d", err);
	return err;
}

static ssize_t
dsfs_listxattr(struct dentry *dentry, char *list, size_t size)
{
	struct dentry *h_dentry = NULL;
	int err = -ENOTSUPP;
	char *encoded_list = NULL;

	DSFS_DBG("");

	h_dentry = dsfs_h_dentry(dentry);

	BUG_ON(!h_dentry);
	BUG_ON(!h_dentry->d_inode);
	BUG_ON(!h_dentry->d_inode->i_op);

	if (h_dentry->d_inode->i_op->listxattr) {
		encoded_list = list;
		mutex_lock(&h_dentry->d_inode->i_mutex);
		err = h_dentry->d_inode->i_op->listxattr(h_dentry, encoded_list, size);
		mutex_unlock(&h_dentry->d_inode->i_mutex);
	}

	DSFS_DBG("err = %d", err);
	return err;
}

struct inode_operations dsfs_symlink_iops = {
	.readlink = dsfs_readlink,
	.follow_link = dsfs_follow_link,
	.put_link = dsfs_put_link,
	.permission = dsfs_permission,
	.setattr = dsfs_setattr,
	.setxattr = dsfs_setxattr,
	.getxattr = dsfs_getxattr,
	.listxattr = dsfs_listxattr,
	.removexattr = dsfs_removexattr
};

struct inode_operations dsfs_dir_iops = {
	.create = dsfs_create,
	.lookup = dsfs_lookup,
	.link = dsfs_link,
	.unlink = dsfs_unlink,
	.symlink = dsfs_symlink,
	.mkdir = dsfs_mkdir,
	.rmdir = dsfs_rmdir,
	.mknod = dsfs_mknod,
	.rename = dsfs_rename,
	.permission = dsfs_permission,
	.setattr = dsfs_setattr,
	.setxattr = dsfs_setxattr,
	.getxattr = dsfs_getxattr,
	.listxattr = dsfs_listxattr,
	.removexattr = dsfs_removexattr,
};

struct inode_operations dsfs_ctl_iops = {
	.create = dsfs_create,
	.lookup = dsfs_lookup,
	.unlink = dsfs_ctl_unlink,
};

struct inode_operations dsfs_dc_dir_iops = {
	.create =	dsfs_dc_create,
	.lookup =	dsfs_dc_lookup,
	.mkdir =	dsfs_dc_mkdir,
	.unlink =	dsfs_dc_unlink,
	.rmdir =	dsfs_dc_rmdir,

	.link = dsfs_link,
	.symlink = dsfs_symlink,
	.mknod = dsfs_mknod,
	.rename = dsfs_rename,
	.permission = dsfs_permission,
	.setattr = dsfs_setattr,
	.setxattr = dsfs_setxattr,
	.getxattr = dsfs_getxattr,
	.listxattr = dsfs_listxattr,
	.removexattr = dsfs_removexattr,
};

struct inode_operations dsfs_root_iops = {
	.permission = dsfs_permission,
	.setattr = dsfs_setattr,
	.setxattr = dsfs_setxattr,
	.getxattr = dsfs_getxattr,
	.listxattr = dsfs_listxattr,
	.removexattr = dsfs_removexattr,
};
