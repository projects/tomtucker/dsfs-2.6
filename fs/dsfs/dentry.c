/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/fs.h>
#include <linux/exportfs.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_DENTRY

static int dsfs_d_revalidate(struct dentry *dentry, struct nameidata *nd)
{
	struct dentry *h_dentry;
	struct vfsmount *h_mount;
	struct dsfs_nd_args nd_;
	int valid = 1;

	h_dentry = dsfs_h_dentry(dentry);
	if (h_dentry && h_dentry->d_op && h_dentry->d_op->d_revalidate) {
		h_mount = dsfs_h_mnt(dentry);
		dsfs_nd_push_(&nd_, nd, h_dentry, h_mount);
		valid = h_dentry->d_op->d_revalidate(dentry, nd);
		dsfs_nd_pop(&nd_, nd);
	}

	return valid;
}

/* 
 * Call the host fs d_hash method if defined
 */
static int dsfs_d_hash(struct dentry *dentry, struct qstr *name)
{
	struct dentry *h_dentry;

	h_dentry = dsfs_h_dentry(dentry);
	if (h_dentry && h_dentry->d_op && h_dentry->d_op->d_hash)
		return h_dentry->d_op->d_hash(h_dentry, name);

	return 0;
}

/* dsfs_d_compare
 *
 * Call the host fs d_compare method if defined, otherwise mimic the default
 * behavior in dcache.c
 */
static int dsfs_d_compare(struct dentry *dentry, struct qstr *a, struct qstr *b)
{
	struct dentry *h_dentry;

	h_dentry = dsfs_h_dentry(dentry);
	if (h_dentry && h_dentry->d_op && h_dentry->d_op->d_compare)
		return h_dentry->d_op->d_compare(h_dentry, a, b);

	return a->len != b->len || memcmp(a->name, b->name, b->len);
}

/*
 * allow the VFS to enquire as to whether a dentry should be unhashed (mustn't
 * sleep)
 * - called from dput() when d_count is going to 0.
 * - return 1 to request dentry be unhashed, 0 otherwise
 */
static int dsfs_d_delete(struct dentry *dentry)
{

	struct dentry *h_dentry;

	BUG_ON(!DSFS_D(dentry));

	h_dentry = dsfs_h_dentry(dentry);
	if (h_dentry && h_dentry->d_op && h_dentry->d_op->d_delete)
		return h_dentry->d_op->d_delete(h_dentry);

	return 1;
}


void dsfs_d_release(struct dentry *dentry)
{
	struct dentry *h_dentry;
	struct vfsmount *h_mnt;

	if (!DSFS_D(dentry))
		return;

	h_dentry = dsfs_h_dentry(dentry);
	if (h_dentry)
		dput(h_dentry);

	h_mnt = dsfs_h_mnt(dentry);
	if (h_mnt)
		mntput(h_mnt);

	kfree(DSFS_D(dentry));
}

static void dsfs_d_iput(struct dentry *dentry, struct inode *inode)
{
	iput(inode);
}


struct dentry_operations dsfs_dops = {
	.d_revalidate = dsfs_d_revalidate,
	.d_hash = dsfs_d_hash,
	.d_compare = dsfs_d_compare,
	.d_release = dsfs_d_release,
	.d_delete = dsfs_d_delete,
	.d_iput = dsfs_d_iput,
};
