/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/fs.h>
#include <linux/exportfs.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/file.h>
#include <linux/mm_types.h>
#include <linux/smp_lock.h>
#include <linux/gfp.h>
#include <linux/mm.h>
#include <linux/ctype.h>
#include <linux/uaccess.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_LAYOUT_FILE

static int __unmarshal_raw_layout(struct dsfs_layout_ *rlo, struct dsfs_layout *lo)
{
	unsigned int dev_count;
	int i;

	DSFS_DBG("rlo=%p, magic=%08x, version=%08x, stripe_type=%08x, stripe_size=%d, dev_count=%d",
		rlo, be32_to_cpu(rlo->magic), be32_to_cpu(rlo->version),
		be32_to_cpu(rlo->stripe_type), be32_to_cpu(rlo->stripe_size),
		be32_to_cpu(rlo->dev_count));

	if (be32_to_cpu(rlo->magic) != DSFS_LAYOUT_MAGIC) {
		printk(KERN_INFO "dsfs: Invalid magic %08x in layout file.",
		       be32_to_cpu(rlo->magic));
		return -EINVAL;
	}
	dev_count = be32_to_cpu(rlo->dev_count);
	if (dev_count > DSFS_LAYOUT_MAX_DEVS) {
		printk(KERN_INFO
		       "dsfs: Invalid device count %d in layout file.",
		       dev_count);
		return -EINVAL;
	}
	lo->version = be32_to_cpu(rlo->version);
	lo->fsize = be64_to_cpu(rlo->fsize);
	lo->stripe_type = be32_to_cpu(rlo->stripe_type);
	lo->stripe_size = be32_to_cpu(rlo->stripe_size);
	lo->dev_count = be32_to_cpu(rlo->dev_count);
	for (i = 0; i < dev_count; i++)
		lo->dev_list[i] = be32_to_cpu(rlo->dev_list[i]);

	return 0;
}

static void __marshal_raw_layout(struct dsfs_layout *lo, struct dsfs_layout_ *rlo)
{
	size_t dev;

	DSFS_DBG("lo=%p, stripe_type=%08x, stripe_size=%zu, dev_count=%zu",
		lo, lo->stripe_type, lo->stripe_size, lo->dev_count);

	rlo->magic = cpu_to_be32(DSFS_LAYOUT_MAGIC);
	rlo->version = cpu_to_be32(DSFS_LAYOUT_VERSION);
	rlo->fsize = cpu_to_be64(lo->fsize);
	rlo->stripe_type = cpu_to_be32(lo->stripe_type);
	rlo->stripe_size = cpu_to_be32(lo->stripe_size);
	rlo->dev_count = cpu_to_be32(lo->dev_count);
	for (dev = 0; dev < lo->dev_count; dev++)
		rlo->dev_list[dev] = cpu_to_be32(lo->dev_list[dev]);
}

static int __read_raw_layout(struct file *filp, struct dsfs_layout_ *rlo)
{
	mm_segment_t oldfs;
	int err;
	loff_t pos;

	pos = 0;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = filp->f_op->read(filp, (char __user *)rlo, sizeof(*rlo), &pos);
	DSFS_DBG("err=%d, count=%zu", err, sizeof(*rlo));
	set_fs(oldfs);
	return err;
}

static int __write_raw_layout(struct file *filp, struct dsfs_layout_ *rlo)
{
	mm_segment_t oldfs;
	int err;
	loff_t pos;

	DSFS_DBG("filp=%p, rlo=%p", filp, rlo);
	pos = 0;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = filp->f_op->write(filp, (const char __user *)rlo,
				sizeof(*rlo), &pos);
	set_fs(oldfs);
	DSFS_DBG("err=%d", err);
	return err;
}

static int __init_def_lo(struct dsfs_sb *sbi)
{
	struct dsfs_layout_ *rlo;
	struct file *filp;
	int err = 0;
	int cnt;

	rlo = kmalloc(sizeof(*rlo), GFP_KERNEL);
	if (!rlo)
		return -ENOMEM;

	filp = dsfs_open_file(sbi, DSFS_DEF_LAYOUT_FILE);
	DSFS_DBG("filp=%p", filp);
	if (IS_ERR(filp)) {
		err = PTR_ERR(filp);
		goto out;
	}
	
	/* Attempt to read the default alayout */
	cnt = __read_raw_layout(filp, rlo);
	if (cnt == sizeof(*rlo))
		err = __unmarshal_raw_layout(rlo, sbi->si_def_lo);
	if (cnt != sizeof(*rlo) || err) {
		DSFS_DBG("Initializing default layout cnt=%d, err=%d", cnt, err); 
		rlo->magic = cpu_to_be32(DSFS_LAYOUT_MAGIC);
		rlo->version = cpu_to_be32(DSFS_LAYOUT_VERSION);
		rlo->stripe_type = cpu_to_be32(DSFS_STRIPE_DENSE);
		rlo->stripe_size = cpu_to_be32(DSFS_STRIPE_DEF_SZ);
		rlo->fsize = cpu_to_be32(0);
		rlo->dev_count = 0;
		err = __unmarshal_raw_layout(rlo, sbi->si_def_lo);
		BUG_ON(err);
		err = __write_raw_layout(filp, rlo);
	}
	filp_close(filp, current->files);
 out:
	kfree(rlo);
	DSFS_DBG("err=%d", err);
	return err;
}

int dsfs_def_layout_init(struct dentry *root)
{
	struct dentry *dentry;
	int err;
	struct dsfs_sb *sbi = DSFS_SB(root->d_inode->i_sb);
	size_t sz;
	int init = 0;

	sbi->si_def_lo = kzalloc(sizeof(struct dsfs_layout), GFP_KERNEL);
	if (!sbi->si_def_lo)
		return -ENOMEM;

	/* Lookup the file's dentry */
	dentry = lookup_one_len(DSFS_DEF_LAYOUT_FILE, root,
				strlen(DSFS_DEF_LAYOUT_FILE));
	if (IS_ERR(dentry)) {
		err = PTR_ERR(dentry);
		goto out;
	}
	/* See if the file already exists. If not, create it */
	if (!dentry->d_inode) {
		err = dsfs_create(root->d_inode, dentry, 0644, NULL);
		if (err)
			goto out;
		init = 1;
	}
	err = __init_def_lo(sbi);
	DSFS_DBG("err=%d", err);
	if (err > 0)
		err = 0;

	DSFS_I(dentry->d_inode)->ii_type = DSFS_I_LO_CNTL;
	dentry->d_inode->i_fop = &dsfs_def_layout_fops;
	sz = i_size_read(dsfs_h_inode(dentry->d_inode));
	i_size_write(dentry->d_inode, sz);

 out:
	DSFS_DBG("err=%d", err);
	return err;
}

/*
 * Format a buffer based on the contents of the default layout cached
 * in the DSFS super block for this mount.
 */
ssize_t dsfs_def_lo_read(struct file *file, char __user *buf, size_t count, loff_t *ppos)
{
	int tot, cnt, dev;
	char *stripe_str;
	struct dsfs_sb *sbi;
	char fmt[128];

	/* Any read anywhere except zero returns EOF */
	if (*ppos)
		return 0;

	sbi = DSFS_SB(file->f_dentry->d_inode->i_sb);
	BUG_ON(!sbi);

	switch (sbi->si_def_lo->stripe_type) {
	case DSFS_ROUND_ROBIN:
		stripe_str = "round_robin";
		break;
	case DSFS_STRIPE_DENSE:
		stripe_str = "stripe_dense";
		break;
	case DSFS_STRIPE_SPARSE:
		stripe_str = "stripe_sparse";
		break;
	default:
		stripe_str = "<unknown>";
	}
	cnt = snprintf(fmt, sizeof(fmt), "%-14s : %08x\n", "layout_version",
		       sbi->si_def_lo->version);
	if (cnt > count || copy_to_user(buf, fmt, cnt))
		return -EFAULT;
	tot = cnt;
	count -= cnt;
	cnt = snprintf(fmt, sizeof(fmt), "%-14s : %s\n", "stripe_type",
		       stripe_str);
	if (cnt > count || copy_to_user(&buf[tot], fmt, cnt))
		return -EFAULT;
	tot += cnt;
	count -= cnt;
	cnt = snprintf(fmt, sizeof(fmt), "%-14s : %zu\n", "stripe_size",
		       sbi->si_def_lo->stripe_size);
	if (cnt > count || copy_to_user(&buf[tot], fmt, cnt))
		return -EFAULT;
	tot += cnt;
	count -= cnt;
	cnt = snprintf(fmt, sizeof(fmt), "%-14s : %zu\n", "dev_count",
		       sbi->si_def_lo->dev_count);
	if (cnt > count || copy_to_user(&buf[tot], fmt, cnt))
		return -EFAULT;
	tot += cnt;
	count -= cnt;
	cnt = snprintf(fmt, sizeof(fmt), "%-14s : ", "dev_list");
	if (cnt > count || copy_to_user(&buf[tot], fmt, cnt))
		return -EFAULT;
	tot += cnt;
	count -= cnt;
	for (dev = 0; dev < sbi->si_def_lo->dev_count; dev++) {
		cnt = snprintf(fmt, sizeof(fmt), "%d ",
			       sbi->si_def_lo->dev_list[dev]);
		if (cnt > count || copy_to_user(&buf[tot], fmt, cnt))
			return -EFAULT;
		tot += cnt;
		count -= cnt;
	}
	cnt = snprintf(fmt, sizeof(fmt), "\n");
	if (cnt > count || copy_to_user(&buf[tot], fmt, cnt))
		return -EFAULT;
	tot += cnt;
	file->f_pos = *ppos = tot;
	return tot;
}

static int add_dev(struct dsfs_sb *sbi, int dev)
{
	int i;
	int *dev_list = sbi->si_def_lo->dev_list;
	int dev_count = sbi->si_def_lo->dev_count;

	/* Check if we're over the max devs */
	if (dev_count == DSFS_LAYOUT_MAX_DEVS)
		return -ENOMEM;

	/* See if the device already exists */
	for (i = 0; i < dev_count; i++)
		if (dev_list[i] == dev)
			return -EEXIST;

	/* Add the new device and bump the count */
	dev_list[dev_count] = dev;
	sbi->si_def_lo->dev_count = dev_count + 1;
	return 0;
}

static int rem_dev(struct dsfs_sb *sbi, int dev)
{
	int i;
	int *dev_list = sbi->si_def_lo->dev_list;
	int dev_count = sbi->si_def_lo->dev_count;

	/* Check if we've got at least one */
	if (dev_count == 0)
		return -ENODEV;

	/* Find the device */
	for (i = 0; i < dev_count; i++)
		if (dev_list[i] == dev)
			break;

	/* If we didn't find it, return error */
	if (i == dev_count)
		return -ENODEV;

	/* Move all the other devices down */
	for (; i < dev_count-1; i++)
		dev_list[i] = dev_list[i+1];

	sbi->si_def_lo->dev_count = dev_count - 1;
	return 0;
}

/*
 * add_dev <dev_id>
 * rem_dev <dev_id>
 * stripe_size <size>
 * stripe_type ["round_robin"|"dense"|"sparse"]
 */
static int parse_def_lo_buf(struct dsfs_sb *sbi, const char *buf, size_t count)
{
	int err, cnt, idx, dev;
	size_t stripe_size;
	char type[32];

	DSFS_DBG("count=%zu, buf=\n\"%*.s\"", count, (int)count, buf);
	cnt = sscanf(buf, "add_dev %d%n", &dev, &idx);
	DSFS_DBG("cnt=%d", cnt);
	if (cnt == 1) {
		err = add_dev(sbi, dev);
		if (err)
			return err;
		goto out;
	}
	cnt = sscanf(buf, "rem_dev %d%n", &dev, &idx);
	DSFS_DBG("cnt=%d", cnt);
	if (cnt == 1) {
		err = rem_dev(sbi, dev);
		if (err)
			return err;
		goto out;
	}
	cnt = sscanf(buf, "stripe_type %32s%n", type, &idx);
	DSFS_DBG("cnt=%d", cnt);
	if (cnt == 1) {
		if (strcmp(type, "round_robin") == 0)
			sbi->si_def_lo->stripe_type = DSFS_ROUND_ROBIN;
		else if (strcmp(type, "dense") == 0)
			sbi->si_def_lo->stripe_type = DSFS_STRIPE_DENSE;
		else if (strcmp(type, "sparse") == 0)
			sbi->si_def_lo->stripe_type = DSFS_STRIPE_SPARSE;
		else
			return -EINVAL;
		goto out;
	}
	cnt = sscanf(buf, "stripe_size %zu%n", &stripe_size, &idx);
	DSFS_DBG("cnt=%d", cnt);
	if (cnt == 1) {
		sbi->si_def_lo->stripe_size = stripe_size;
		goto out;
	}
	DSFS_DBG("parse_error cnt=%d", cnt);
	return -EINVAL;

 out:
	return 0;
}

int dsfs_write_layout_kern(struct file *filp, struct dsfs_layout *lo)
{
	struct dsfs_layout_ *rlo;
	int err;

	DSFS_DBG("filp=%p", filp);
	rlo = kmalloc(sizeof(*rlo), GFP_KERNEL);
	if (!rlo)
		return -ENOMEM;

	__marshal_raw_layout(lo, rlo);
	err = __write_raw_layout(filp, rlo);
	kfree(rlo);
	return err;
}

/**
 * dsfs_read_layout_kern - read the layout file into memory
 *
 * @filp: File ptr for the layout file.
 * @lo: Ptr to kernel memory layout structure
 *
 * Returns:
 * 0	- Success,
 * !0	- Error reading layout file.
 **/
int dsfs_read_layout_kern(struct file *filp, struct dsfs_layout *lo)
{
	struct dsfs_layout_ *rlo;
	int err;

	DSFS_DBG("filp=%p", filp);
	rlo = kmalloc(sizeof(*rlo), GFP_KERNEL);
	if (!rlo)
		return -ENOMEM;

	err = __read_raw_layout(filp, rlo);
	__unmarshal_raw_layout(rlo, lo);
	kfree(rlo);
	if (err < 0)
		return err;

	return 0;
}

static int dsfs_write_layout(struct file *filp, struct dsfs_layout *lo, char __user *buf)
{
	struct dsfs_layout_ *rlo = (struct dsfs_layout_ *)buf;
	loff_t pos;
	int err;

	DSFS_DBG("filp=%p, rlo=%p", filp, rlo);

	__marshal_raw_layout(lo, rlo);
	pos = 0;
	err = filp->f_op->write(filp, (const char __user *)rlo,
				sizeof(*rlo), &pos);
 
	DSFS_DBG("err=%d", err);
	return err;
}

/*
 * Handles writes to a layout file. The syntax is as follows:
 *
 * add_dev INTEGER
 * rem_dev INTEGER
 * stripe_type dense
 * stripe_size INTEGER
 *
 * This file also updates the default layout pointed to by our super block
 */
ssize_t dsfs_def_lo_write(struct file *file, const char __user *buf,
			   size_t count, loff_t *ppos)
{
	int err = -ENOSYS;
	struct file *h_file;
	struct inode *inode;
	struct inode *h_inode;
	struct dsfs_sb *sbi;

	DSFS_DBG("file=%p, buf=%p, count=%zu, *ppos=%llu",
		file, buf, count, *ppos);

	if (!count)
		return 0;

	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);

	inode = file->f_dentry->d_inode;
	h_inode = dsfs_h_inode(inode);
	sbi = DSFS_SB(file->f_dentry->d_inode->i_sb);
	BUG_ON(!sbi);

	if (!h_file->f_op || !h_file->f_op->write)
		goto out;

	/* Parse the write to make sure it's ok */
	err = parse_def_lo_buf(sbi, buf, count);
	if (err)
		printk("Invalid syntax for DEFAULT_LAYOUT file write\n");

	err = dsfs_write_layout(h_file, sbi->si_def_lo, (char *)buf);

	/* update this inode's size */
	i_size_write(inode, i_size_read(h_inode));
 out:
	return err;
}

