/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/fs.h>
#include <linux/exportfs.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/file.h>
#include <linux/mm_types.h>
#include <linux/smp_lock.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_DC_INODE

/*
 * __dc_inode_attach - Attach a DSFS inode to a host FS inode.
 *
 * @h_dentry: host fs dentry
 * @dentry: my dentry for new file.
 * @sb: my sb
 * @ppi: a pointer to store new inode. May be null.
 *
 * - Create a DSFS inode that is based on the host dentry provided by
 *   h_dentry.
 * - If this is a directory, read it's dev_list, inherit size from h_inode
 * - If this is a file, read it's layout and update new inode size
 */
static int __dc_inode_attach(struct inode *dir,
			     struct dentry *h_dentry, struct dentry *dentry,
			     struct super_block *sb, struct inode **ppi)
{
	int err = 0;
	struct inode *h_inode;
	struct inode *inode;
	struct dsfs_inode *dc_inode;

	DSFS_DBG("h_dentry=%p, dentry=%p, sb=%p, ppi=%p",
		h_dentry, dentry, sb, ppi);

	BUG_ON(!dentry);
	h_inode = h_dentry->d_inode;
	BUG_ON(!h_inode);

	/* Check that h_dentry didn't cross a mount point */
	if (h_inode->i_sb != DSFS_SB(sb)->si_h_sb) {
		DSFS_DBG("crossed mount point!");
		return -EXDEV;
	}

	inode = dsfs_iget(sb, h_inode->i_ino);
	DSFS_DBG("DC inode=%p, LO inode=%p", inode, h_inode);
	if (!inode || IS_ERR(inode))
		return -ENOMEM;

	/* Attach the host inode to our inode */
	dc_inode = DSFS_I(inode);
	dc_inode->ii_h_inode = igrab(h_inode);
	dc_inode->ii_type = DSFS_I_DC;

	/* Setup our inode ops */
	if (S_ISREG(h_inode->i_mode)) {
		inode->i_op = &dsfs_dc_dir_iops;
		inode->i_fop = &dsfs_dc_fops;
	} else if (S_ISLNK(h_inode->i_mode)) {
		inode->i_op = &dsfs_symlink_iops;
	} else if (S_ISDIR(h_inode->i_mode)) {
		inode->i_op = &dsfs_dc_dir_iops;
		inode->i_fop = &dsfs_dir_fops;
	} else if (S_ISCHR(h_inode->i_mode) ||
		   S_ISBLK(h_inode->i_mode) ||
		   S_ISFIFO(h_inode->i_mode) ||
		   S_ISSOCK(h_inode->i_mode)) {
		init_special_inode(inode, h_inode->i_mode, h_inode->i_rdev);
	}
	inode->i_mapping->a_ops = h_inode->i_mapping->a_ops;
	if (ppi)
		*ppi = inode;

	/* If this is a directory, read the dev list */
	if (S_ISDIR(h_dentry->d_inode->i_mode))
		err = dsfs_read_dev_list(dir, dentry, inode);
	else if (S_ISREG(h_dentry->d_inode->i_mode))
		err = dsfs_read_layout(dentry, inode);
	if (err)
		goto out;

	/* We now have a layout/dev_list, copy the attributes */
	dsfs_dc_copy_attr_all(inode, h_dentry->d_inode);

 out:
	return err;
}

/*
 * __dc_create - create a regular non-shared file. This is a
 * convenience function used to create stripe files in device
 * directories.
 */
static int __dc_create(struct inode *dir, struct dentry *dentry, int mode)
{
	int err;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;

	DSFS_DBG("dir=%p, dentry=%p, mode=%08x, name=%s",
		dir, dentry, mode, dentry->d_name.name);

	h_dentry = dsfs_h_dentry(dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);

	err = vfs_create(h_dir_dentry->d_inode, h_dentry, mode, NULL);
	if (err)
		goto out;
	err = dsfs_inode_attach(h_dentry, dentry, dir->i_sb, NULL);
	if (err)
		goto out;

 out:
	unlock_dir(h_dir_dentry);
	DSFS_DBG("err=%d", err);
	return err;
}

/**
 * dsfs_create - create a regular file
 *
 * @dir: The directory inode to contain the file
 * @dentry: The dentry for the new file
 * @mode: file persmissions
 * @nd: nameidate structure for the new file
 *
 * Creates :
 *
 * - The laytout file in the DC directory that contains the file's
 *   size and shares it's access times and rights with the "real"
 *   file.
 *
 * - A file in each device directory specified in the layout to
 *   contain the device's data stripes.
 */
int dsfs_dc_create(struct inode *dir, struct dentry *dentry, int mode,
		    struct nameidata *nd)
{
	int err;
	struct dentry *h_dentry;
	struct vfsmount *h_mount;
	struct dentry *h_dir_dentry;
	struct dsfs_nd_args nd_;
	struct dsfs_inode *dc_dir;
	struct inode *inode;
	struct dsfs_inode *dc_inode;
	struct file *filp;
	struct dsfs_sb *sbi;
	int i;

	DSFS_DBG("dir=%p, dentry=%p, mode=%08x, name=%.*s",
		dir, dentry, mode,
		(nd?nd->path.dentry->d_name.len:10),
		(nd?(char *)nd->path.dentry->d_name.name:"<nul>"));

	sbi = DSFS_SB(dir->i_sb);
	if (sbi->si_state != DSFS_READY)
		return -EINVAL;

	h_dentry = dsfs_h_dentry(dentry);
	h_mount = dsfs_h_mnt(dentry);

	/* Create the LAYOUT file in the DC directory */
	h_dir_dentry = dget(h_dentry->d_parent);
	mutex_lock(&h_dir_dentry->d_inode->i_mutex);

	dsfs_nd_push_(&nd_, nd, h_dentry, h_mount);
	err = vfs_create(h_dir_dentry->d_inode, h_dentry, mode, nd);
	dsfs_nd_pop(&nd_, nd);
	if (err)
		goto out;

	/* Write the DC file with the default layout */
	filp = dentry_open(dget(h_dentry), mntget(h_mount), O_RDWR);
	dsfs_write_layout_kern(filp, sbi->si_def_lo);
	filp_close(filp, current->files);
	DSFS_DBG("dentry->d_count=%d", atomic_read(&h_dentry->d_count));

	/* Attach a new inode to the host inode */
	err = __dc_inode_attach(dir, h_dentry, dentry, dir->i_sb, &inode);
	if (err)
		goto out;
	d_instantiate(dentry, inode);

	/* Create empty device stripe files */
	dc_inode = DSFS_I(inode);
	dc_dir = DSFS_I(dir);
	for (i = 0; i < dc_inode->ii_layout->dev_count; i++) {
		int dev_id;
		struct dentry *d;
		struct dentry *dc;
		dev_id = dc_inode->ii_layout->dev_list[i];
		d = dc_dir->ii_dev_list[dev_id];
		if (!d) {
			printk(KERN_INFO
			       "dsfs: Device %d is not initialized.\n", dev_id);
			continue;
		}
		DSFS_DBG("i=%d, dev_id=%d, d=%p", i, dev_id, d);
		dc = lookup_one_len(dentry->d_name.name, d, dentry->d_name.len);
		DSFS_DBG("dc=%p", dc);
		if (IS_ERR(dc)) {
			printk(KERN_INFO "dsfs: Error looking up stripe file %s\n",
			       dentry->d_name.name);
			continue;
		}
		if (dc->d_inode) {
			printk(KERN_INFO
			       "dsfs: stripe file %s on device %d "
			       "already exists\n", dentry->d_name.name, dev_id);
			goto next;
		}
		err = __dc_create(d->d_inode, dc, mode);
		if (err)
			printk(KERN_INFO
			       "dsfs: Error %d creating file %s on device %d.\n",
			       err, dentry->d_name.name, dev_id);
	next:
		dput(dc);
		DSFS_DBG("dc->d_count=%d", atomic_read(&dc->d_count));
	}
	err = 0;

 out:
	mutex_unlock(&h_dir_dentry->d_inode->i_mutex);
	dput(h_dir_dentry);
	DSFS_DBG("err=%d", err);
	return err;
}

/*
 * __dc_unlink - Remove a file
 */
static int __dc_unlink(struct inode *dir, struct dentry *dentry)
{
	int err = 0;
	struct inode *h_dir;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;

	DSFS_DBG("");

	h_dir = dsfs_h_inode(dir);
	h_dentry = dsfs_h_dentry(dentry);

	BUG_ON(!h_dentry);

	dget(dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);

	dget(h_dentry);
	err = vfs_unlink(h_dir, h_dentry);
	dput(h_dentry);
	if (!err)
		d_delete(h_dentry);

	dsfs_copy_attr_times(dir, h_dir);
	dentry->d_inode->i_nlink = dsfs_h_inode(dentry->d_inode)->i_nlink;
	dentry->d_inode->i_ctime = dir->i_ctime;

	unlock_dir(h_dir_dentry);

	if (!err)
		d_drop(dentry);

	dput(dentry);
	return err;
}

int dsfs_dc_unlink(struct inode *dir, struct dentry *dentry)
{
	int err = 0;
	struct inode *h_dir;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;
	struct dsfs_inode *dc_inode;
	struct dsfs_inode *dc_dir;
	struct dsfs_sb *sbi;
	int i;

	DSFS_DBG("");

	sbi = DSFS_SB(dir->i_sb);
	if (sbi->si_state != DSFS_READY)
		return -EINVAL;

	/* Unlink data server stripe files */
	dc_inode = DSFS_I(dentry->d_inode);
	dc_dir = DSFS_I(dir);
	for (i = 0; i < sbi->si_dev_list->dev_count; i++) {
		int dev_id;
		struct dentry *dev_dir;
		struct dentry *dev_stripe;
		BUG_ON(!dc_inode->ii_layout);
		dev_id = dc_inode->ii_layout->dev_list[i];
		dev_dir = dc_dir->ii_dev_list[i];
		if (!dev_dir) {
			printk(KERN_INFO
			       "dsfs: device id %d not initialized.", i);
			continue;
		}
		DSFS_DBG("i=%d, dev_id=%d, dev_dir=%p", i, dev_id, dev_dir);
		dev_stripe = lookup_one_len(dentry->d_name.name, dev_dir,
					    dentry->d_name.len);
		DSFS_DBG("dev_stripe=%p", dev_stripe);
		if (IS_ERR(dev_stripe)) {
			printk(KERN_INFO
			       "dsfs: Error looking up stripe file %s\n",
			       dentry->d_name.name);
			continue;
		}
		if (!dev_stripe->d_inode) {
			printk(KERN_INFO
			       "dsfs: stripe file %s on device %d "
			       "doesn't exist.\n", dentry->d_name.name, i);
			goto next;
		}
		err = __dc_unlink(dev_dir->d_inode, dev_stripe);
		if (err)
			printk(KERN_INFO
			       "dsfs: Error %d destroying file %s on device %d",
			       err, dentry->d_name.name, i);
	next:
		DSFS_DBG("dev_stripe->d_count=%d",
			atomic_read(&dev_stripe->d_count));
		dput(dev_stripe);
	}

	/* Unlink the layout file */
	h_dir = dsfs_h_inode(dir);
	h_dentry = dsfs_h_dentry(dentry);

	dget(dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);

	dget(h_dentry);
	err = vfs_unlink(h_dir, h_dentry);
	dput(h_dentry);
	if (!err)
		d_delete(h_dentry);

	dsfs_copy_attr_times(dir, h_dir);
	dentry->d_inode->i_nlink = dsfs_h_inode(dentry->d_inode)->i_nlink;
	dentry->d_inode->i_ctime = dir->i_ctime;

	unlock_dir(h_dir_dentry);

	if (!err)
		d_drop(dentry);

	dput(dentry);
	return err;
}


/*
 * dsfs_read_dev_list
 *
 * dir: Local directory's parent inode.
 * dentry: Local directory's dentry.
 *
 * Lookup the sibling directory dentry for each device's directory tree.
 */
int dsfs_read_dev_list(struct inode *parent, struct dentry *dentry, struct inode *inode)
{
	struct dsfs_sb *sbi;
	struct dsfs_inode *dc_parent;
	struct dsfs_inode *dc_inode = DSFS_I(inode);
	int i;
	DSFS_DBG("parent %p dentry %p inode %p", parent, dentry, inode);
	sbi = DSFS_SB(parent->i_sb);
	dc_parent = DSFS_I(parent);
	for (i = 0; i < sbi->si_dev_list->dev_count; i++) {
		struct dentry *dev_dir;
		struct dentry *dc;
		dc = dc_inode->ii_dev_list[i];
		if (!dc) {
			dev_dir = dc_parent->ii_dev_list[i];
			if (!dev_dir) {
				printk(KERN_INFO
				       "dsfs: Device %d is not initialized in parent.\n",
				       i);
				continue;
			}
			dc = lookup_one_len(dentry->d_name.name, dev_dir,
					    dentry->d_name.len);
			DSFS_DBG("dc=%p", dc);
			if (IS_ERR(dc)) {
				printk(KERN_INFO "dsfs: Error looking up DS directory %s\n",
				       dentry->d_name.name);
				continue;
			}
			if (dc->d_inode)
				dc_inode->ii_dev_list[i] = dget(dc);
			else
				printk(KERN_INFO
				       "dsfs: directory %s on device %d "
				       "does not exist.\n", dentry->d_name.name, i);
			dput(dc);
			DSFS_DBG("dc->d_count=%d", atomic_read(&dc->d_count));
		}
		DSFS_DBG("dc %p", dc);
	}
	return 0;
}

/**
 * dsfs_read_layout - Creates a layout for the specified inode.
 *
 * @dentry: Ptr to dentry for layout file
 * @inode: DC inode to attach with layout
 *
 * Opens the file referred to by 'dentry', allocates a layout
 * and reads the contents of the layout into the allocated layout. The
 * layout is then attached to the specied 'inode'.
 *
 * Returns:
 * 0	Success
 * !0	Error allocating/reading layout
 **/
int dsfs_read_layout(struct dentry *dentry, struct inode *inode)
{
	struct file *filp;
	struct dentry *h_dentry;
	struct vfsmount *h_mount;
	struct dsfs_inode *dc_inode;
	int err = 0;

	BUG_ON(!dentry);
	BUG_ON(!inode);
	DSFS_DBG("dentry %p inode %p", dentry, inode);

	/* Allocate the layout and attach it to the inode */
	dc_inode = DSFS_I(inode);
	if (!dc_inode->ii_layout)
		dc_inode->ii_layout =
			kzalloc(sizeof(struct dsfs_layout), GFP_KERNEL);
	if (!dc_inode->ii_layout) {
		err = -ENOMEM;
		goto out;
	}
	h_dentry = dsfs_h_dentry(dentry);
	h_mount = dsfs_h_mnt(dentry);
	filp = dentry_open(dget(h_dentry), mntget(h_mount), O_RDONLY);
	if (IS_ERR(filp)) {
		err = PTR_ERR(filp);
		goto out;
	}
	err = dsfs_read_layout_kern(filp, dc_inode->ii_layout);
	filp_close(filp, current->files);
	i_size_write(inode, dc_inode->ii_layout->fsize);
 out:
	return err;
}

/**
 * dsfs_dc_lookup
 *
 * @dir: inode of parent directory
 * @dentry: dentry to use (d_alloc'd with d_parent set)
 *
 * In addition to lookup, this function also populates the device list
 * for directories and the layout for normal files.
 **/
struct dentry *dsfs_dc_lookup(struct inode *dir, struct dentry *dentry,
			      struct nameidata* nd)
{
	int err = 0;
	struct dentry *h_dir_dentry;
	struct dentry *h_dentry;
	struct inode *inode;
	struct vfsmount *h_mnt;
	struct dsfs_sb *sbi;

	DSFS_DBG("dentry->d_name.name=%s", dentry->d_name.name);

	sbi = DSFS_SB(dir->i_sb);
	if (sbi->si_state != DSFS_READY)
		return NULL;

	/* Initialize DC dentry ops */
	dentry->d_op = &dsfs_dops;
	h_dir_dentry = DSFS_D(dentry->d_parent)->di_h_dentry;

	/* Lookup LO dentry using host FS parent directory. */
	mutex_lock(&h_dir_dentry->d_inode->i_mutex);
	h_dentry = lookup_one_len(dentry->d_name.name, h_dir_dentry,
				  dentry->d_name.len);
	mutex_unlock(&h_dir_dentry->d_inode->i_mutex);
	if (IS_ERR(h_dentry))
		return h_dentry;

	/* Update DC parent directory's access time */
	dir->i_atime = h_dir_dentry->d_inode->i_atime;

	/* Alloc private dentry */
	DSFS_DBG("d_fsdata %p", dentry->d_fsdata);
	dentry->d_fsdata = kzalloc(sizeof(struct dsfs_dentry), GFP_KERNEL);
	if (!dentry->d_fsdata) {
		err = -ENOMEM;
		goto err1;
	}
	/* Take a ref on the mnt */
	h_mnt = mntget(dsfs_h_mnt(dentry->d_parent));

	/*
	 * Initialize private dentry. We have a ref on the dentry and
	 * on the mount
	 */
	DSFS_D(dentry)->di_h_dentry = h_dentry;
	DSFS_D(dentry)->di_h_mnt = h_mnt;

	inode = h_dentry->d_inode;
	if (!inode)
		/* Add negative dentry (i.e. not found) */
		goto out;

	/* Allocate a new inode and attach it to the dentry */
	err = __dc_inode_attach(dir, h_dentry, dentry, dir->i_sb, &inode);
	if (err)
		goto err2;

 out:
	return d_splice_alias(inode, dentry);

 err2:
	mntput(h_mnt);
 err1:
	kfree(DSFS_D(dentry));
	dentry->d_fsdata = NULL;
	d_drop(dentry);
	return ERR_PTR(err);
}

/*
 * __dc_mkdir - Create a non-distributed sub-directory in a device
 *		directory
 */
static int __dc_mkdir(struct inode *dir, struct dentry *dentry, int mode)
{
	int err;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;

	DSFS_DBG("");

	h_dentry = dsfs_h_dentry(dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);

	err = vfs_mkdir(h_dir_dentry->d_inode, h_dentry, mode);
	if (err || !h_dentry->d_inode)
		goto out;

	err = dsfs_inode_attach(h_dentry, dentry, dir->i_sb, NULL);

 out:
	unlock_dir(h_dir_dentry);
	if (!dentry->d_inode)
		d_drop(dentry);

	DSFS_DBG("err = %d", err);
	return err;
}

int dsfs_dc_mkdir(struct inode *dir, struct dentry *dentry, int mode)
{
	int err;
	struct dsfs_inode *dc_dir;
	struct dsfs_inode *dc_inode;
	struct inode *inode;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;
	struct dsfs_sb *sbi;
	int i;

	DSFS_DBG("dentry->d_name.name=%s",dentry->d_name.name);

	sbi = DSFS_SB(dir->i_sb);
	if (sbi->si_state != DSFS_READY)
		return -EINVAL;

	/* Create the DC sub-directory */
	h_dentry = dsfs_h_dentry(dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);
	err = vfs_mkdir(h_dir_dentry->d_inode, h_dentry, mode);
	unlock_dir(h_dir_dentry);
	if (err || !h_dentry->d_inode)
		goto out;

	err = __dc_inode_attach(dir, h_dentry, dentry, dir->i_sb, &inode);
	if (err)
		goto out;
	d_instantiate(dentry, inode);

	/* Create the device directories */
	dc_dir = DSFS_I(dir);
	dc_inode = DSFS_I(dentry->d_inode);
	DSFS_DBG("dc_dir %p dc_inode %p", dc_dir, dc_inode);
	for (i = 0; i < sbi->si_dev_list->dev_count; i++) {
		struct dentry *d;
		struct dentry *dc;

		dc_inode->ii_dev_list[i] = NULL;
		d = dc_dir->ii_dev_list[i];
		if (!d) {
			printk(KERN_INFO
			       "dsfs: Device %d is not initialized.\n", i);
			continue;
		}
		dc = lookup_one_len(dentry->d_name.name, d, dentry->d_name.len);
		DSFS_DBG("dc=%p", dc);
		if (IS_ERR(dc)) {
			printk(KERN_INFO "dsfs: Error looking up DS directory %s\n",
			       dentry->d_name.name);
			continue;
		}
		if (dc->d_inode) {
			printk(KERN_INFO
			       "dsfs: directory %s on device %d "
			       "already exists\n", dentry->d_name.name, i);
			goto next;
		}
		BUG_ON(!d->d_inode);
		err = __dc_mkdir(d->d_inode, dc, mode);
		if (err)
			printk(KERN_INFO
			       "dsfs: Error %d creating directory %s on device %d\n",
			       err, dentry->d_name.name, i);
	next:
		dc_inode->ii_dev_list[i] = dget(dc);
		dput(dc);
		DSFS_DBG("dc->d_count=%d", atomic_read(&dc->d_count));
	}

out:
	DSFS_DBG("err = %d", err);
	return err;
}

int dsfs_dc_rmdir(struct inode *dir, struct dentry *dentry)
{
	int err = 0;
	struct dentry *h_dentry;
	struct dentry *h_dir_dentry;
	struct dsfs_sb *sbi;

	DSFS_DBG("");
	sbi = DSFS_SB(dir->i_sb);
	if (sbi->si_state != DSFS_READY)
		return -EINVAL;

	h_dentry = dsfs_h_dentry(dentry);

	dget(dentry);
	h_dir_dentry = dsfs_lock_parent(h_dentry);

	dget(h_dentry);
	err = vfs_rmdir(h_dir_dentry->d_inode, h_dentry);
	dput(h_dentry);

	if (!err)
		d_delete(h_dentry);

	dsfs_copy_attr_times(dir, h_dir_dentry->d_inode);
	dir->i_nlink =  h_dir_dentry->d_inode->i_nlink;

	unlock_dir(h_dir_dentry);

	if (!err)
		d_drop(dentry);

	dput(dentry);

	DSFS_DBG("err = %d", err);
	return err;
}
