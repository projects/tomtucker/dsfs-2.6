/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/fs.h>
#include <linux/exportfs.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/file.h>
#include <linux/mm_types.h>
#include <linux/smp_lock.h>
#include <linux/uaccess.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_FILE_OPS

static loff_t dsfs_llseek(struct file *file, loff_t offset, int origin)
{
	loff_t err;
	struct file *h_file;

	DSFS_DBG("file=%p, offset=%llu, orgin=%d", file, offset, origin);
	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);

	if (h_file->f_op && h_file->f_op->llseek)
		err = h_file->f_op->llseek(h_file, offset, origin);
	else
		err = generic_file_llseek(h_file, offset, origin);

	if (err < 0)
		goto out;

	/* copy file position */
	file->f_pos = h_file->f_pos;

	/* copy read ahead state */
	memcpy(&(file->f_ra), &(h_file->f_ra), sizeof(struct file_ra_state));

	/* update file version */
	file->f_version = h_file->f_version;
 out:
	return err;
}

static ssize_t dsfs_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	int err = -EINVAL;
	struct file *h_file;

	DSFS_DBG("file=%p, buf=%p, count=%zu, ppos=%p", file, buf, count, ppos);

	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);

	if (!h_file->f_op || !h_file->f_op->read)
		goto out;

	err = h_file->f_op->read(h_file, buf, count, ppos);
	if (err >= 0)
		file->f_dentry->d_inode->i_atime =
			h_file->f_dentry->d_inode->i_atime;

	file->f_pos = *ppos;
	memcpy(&(file->f_ra), &(h_file->f_ra), sizeof(struct file_ra_state));

out:
	return err;
}

static ssize_t dsfs_write(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
	int err = 0;
	struct file *h_file;
	struct inode *inode;
	struct inode *h_inode;
	loff_t pos = *ppos;

	DSFS_DBG("file=%p, buf=%p, count=%zu, *ppos=%llu",
		file, buf, count, *ppos);
	if (!count)
		goto out;

	h_file = dsfs_h_file(file);
	BUG_ON(!h_file || !h_file->f_op || !h_file->f_op->write);

	inode = file->f_dentry->d_inode;
	h_inode = dsfs_h_inode(inode);

	/* adjust for append -- seek to the end of the file */
	if ((file->f_flags & O_APPEND))
		pos = i_size_read(inode);

	err = h_file->f_op->write(h_file, buf, count, &pos);
	if (err >= 0) {
		*ppos = pos;
		dsfs_copy_attr_times(inode, h_inode);
		file->f_pos = h_file->f_pos = pos;
	}

	/* update this inode's size */
	if (pos > i_size_read(inode))
		i_size_write(inode, pos);
out:
	return err;
}

static int dsfs_readdir(struct file *file, void *dirent, filldir_t filldir)
{
	int err = -ENOTDIR;
	struct file *h_file;
	struct inode *inode;

	DSFS_DBG("file=%p, dirent=%p", file, dirent);
	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);

	inode = file->f_dentry->d_inode;

	h_file->f_pos = file->f_pos;
	err = vfs_readdir(h_file, filldir, dirent);
	file->f_pos = h_file->f_pos;
	if (err >= 0)
		inode->i_atime = h_file->f_dentry->d_inode->i_atime;

	return err;
}

static int dsfs_ioctl(struct inode *inode, struct file *file,
		       unsigned int cmd, unsigned long arg)
{
	int err = -EINVAL;
	struct file *h_file;
	DSFS_DBG("inode=%p, file=%p, cmd=%d, arg=%lu",
		inode, file, cmd, arg);

	h_file = dsfs_h_file(file);
	if (h_file && h_file->f_op && h_file->f_op->ioctl)
		err = h_file->f_op->ioctl(dsfs_h_inode(inode), h_file, cmd, arg);

	return err;
}

static int dsfs_mmap(struct file *file, struct vm_area_struct *vma)
{
	int err = -EINVAL;
	struct file *h_file;

	DSFS_DBG("file=%p, vma=%p", file, vma);

	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);
	if (h_file->f_op && h_file->f_op->mmap) {
		vma->vm_file = h_file;
		err = h_file->f_op->mmap(h_file, vma);
		get_file(h_file);
		fput(file);
	}
	return err;
}


static int dsfs_open(struct inode *inode, struct file *file)
{
	struct file *h_file;
	struct dentry *h_dentry;
	struct vfsmount *h_mnt;

	DSFS_DBG("inode=%p, file=%p, name=%s", inode, file,
		file->f_dentry->d_name.name);
	file->private_data = kzalloc(sizeof(struct dsfs_file),
				     GFP_KERNEL);
	if (!file->private_data)
		return -ENOMEM;

	h_dentry = dsfs_h_dentry(file->f_dentry);
	dget(h_dentry);

	/*
	 * dentry_open will decrement mnt refcnt if err.
	 * otherwise fput() will do an mntput() for us upon file close.
	 */
	h_mnt = dsfs_h_mnt(file->f_dentry);
	BUG_ON(!h_mnt);
	mntget(h_mnt);
	h_file = dentry_open(h_dentry, h_mnt, file->f_flags);
	if (IS_ERR(h_file))
		goto err_out;

	DSFS_F(file)->fi_h_file = h_file;
	return 0;

err_out:
	kfree(file->private_data);
	return PTR_ERR(h_file);
}


static int dsfs_flush(struct file *file, fl_owner_t id)
{
	int err = 0;
	struct file *h_file;

	DSFS_DBG("file=%p", file);

	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);

	if (h_file->f_op && h_file->f_op->flush)
		err = h_file->f_op->flush(h_file, id);

	return err;
}

static int dsfs_release(struct inode *inode, struct file *file)
{
	DSFS_DBG("inode=%p, file=%p", inode, file);

	fput(dsfs_h_file(file));
	kfree(file->private_data);
	return 0;
}


static int dsfs_fsync(struct file *file, struct dentry *dentry, int datasync)
{
	int err = -EINVAL;
	struct file *h_file;
	struct dentry *h_dentry = dsfs_h_dentry(dentry);

	DSFS_DBG("file=%p, dentry=%p, datasync=%d", file, dentry, datasync);

	/*
	 * When exporting upper file system through NFS with the sync
	 * option, nfsd_sync_dir() sets struct file as NULL. Use
	 * inode's i_fop->fsync instead of file's.  see fs/nfsd/vfs.c
	 */
	if (file == NULL) {
		if (h_dentry->d_inode->i_fop && h_dentry->d_inode->i_fop->fsync) {
			mutex_lock(&h_dentry->d_inode->i_mutex);
			err = h_dentry->d_inode->i_fop->fsync(NULL, h_dentry, datasync);
			mutex_unlock(&h_dentry->d_inode->i_mutex);
		}
	} else {
		h_file = dsfs_h_file(file);
		if (h_file && h_file->f_op && h_file->f_op->fsync) {
			mutex_lock(&h_dentry->d_inode->i_mutex);
			err = h_file->f_op->fsync(h_file, h_dentry, datasync);
			mutex_unlock(&h_dentry->d_inode->i_mutex);
		}
	}
	return err;
}

static int dsfs_fasync(int fd, struct file *file, int flag)
{
	int err = 0;
	struct file *h_file = dsfs_h_file(file);

	DSFS_DBG("fd=%d, file=%p, flag=%d", fd, file, flag);
	BUG_ON(!h_file);

	if (h_file->f_op && h_file->f_op->fasync)
		err = h_file->f_op->fasync(fd, h_file, flag);

	return err;
}

#if 0
static inline void __locks_delete_block(struct file_lock *waiter)
{
	list_del_init(&waiter->fl_block);
	list_del_init(&waiter->fl_link);
	waiter->fl_next = NULL;
}

static void locks_delete_block(struct file_lock *waiter)
{
	lock_kernel();
	__locks_delete_block(waiter);
	unlock_kernel();
}

static inline int dsfs_posix_lock(struct file *file, struct file_lock *fl, int cmd)
{
    int error;
    for(;;) {
	error = posix_lock_file(file, fl);
	if ((error != -EAGAIN) || (cmd == F_SETLK))
	    break;

	error = wait_event_interruptible(fl->fl_wait, !fl->fl_next);
	if(!error)
	    continue;

	locks_delete_block(fl);
	break;
    }

    return error;
}

static int dsfs_setlk(struct file *file, int cmd, struct file_lock *fl)
{
	int err;
	struct inode *inode, *h_inode;
	struct file *h_file = NULL;

	DSFS_DBG("file=%p, cmd=%d, fl=%p", file, cmd, fl);

	error = -EINVAL;
	BUG_ON(!STRUCT FILEO_PRIVATE(file));
	h_file = STRUCT FILEO_LOWER(file);
	BUG_ON(!h_file);

	inode = file->f_dentry->d_inode;
	h_inode = h_file->f_dentry->d_inode;

	/* Don't allow mandatory locks on files that may be memory mapped
	 * and shared.
	 */
	if (IS_MANDLOCK(h_inode) &&
	    (h_inode->i_mode & (S_ISGID | S_IXGRP)) == S_ISGID &&
	    mapping_writably_mapped(h_file->f_mapping)) {
		error = -EAGAIN;
		goto out;
	}

	if (cmd == F_SETLKW) {
		fl->fl_flags |= FL_SLEEP;
	}

	error = -EBADF;
	switch (fl->fl_type) {
	case F_RDLCK:
		if (!(h_file->f_mode & FMODE_READ))
			goto out;
		break;
	case F_WRLCK:
		if (!(h_file->f_mode & FMODE_WRITE))
			goto out;
		break;
	case F_UNLCK:
		break;
	default:
		error = -EINVAL;
		goto out;
	}

	fl->fl_file = h_file;
	error = security_file_lock(h_file, fl->fl_type);
	if (error)
		goto out;

	if (h_file->f_op && h_file->f_op->lock != NULL) {
		error = h_file->f_op->lock(h_file, cmd, fl);
		if (error)
			goto out;
		goto upper_lock;
	}

	error = dsfs_posix_lock(h_file, fl, cmd);
	if (error)
		goto out;


 upper_lock:
	fl->fl_file = file;
	error = dsfs_posix_lock(file, fl, cmd);
	if (error) {
		fl->fl_type = F_UNLCK;
		fl->fl_file = h_file;
		dsfs_posix_lock(h_file, fl, cmd);
	}

 out:
	print_exit_status(error);
	return error;
}

static int dsfs_getlk(struct file *file, struct file_lock *fl)
{
	int err;
	struct file_lock *tempfl = NULL;c
	struct file_lock cfl;

	DSFS_DBG("file=%p, fl=%p", file, fl);

	if (file->f_op && file->f_op->lock) {
		err = file->f_op->lock(file, F_GETLK, fl);
		if (err < 0)
			goto out;
	} else
		tempfl = (posix_test_lock(file, fl, &cfl) ? &cfl : NULL);

	if (!tempfl)
		fl->fl_type = F_UNLCK;
	else
		memcpy(fl, tempfl, sizeof(struct file_lock));

 out:
	return error;
}

static int dsfs_lock(struct file *file, int cmd, struct file_lock *fl)
{
	int err = -EINVAL;
	struct file *h_file = dsfs_h_file(file);
	struct file_lock *tmpfl = NULL;

	DSFS_DBG("file=%p, cmd=%d, fl=%p", file, cmd, fl);
	BUG_ON(!h_file);

	if (!fl)
		return err;

	fl->fl_file = h_file;
	switch(cmd) {
		case F_GETLK:
		case F_GETLK64:
			err = dsfs_getlk(h_file, fl);
			break;

		case F_SETLK:
		case F_SETLKW:
		case F_SETLK64:
		case F_SETLKW64:
			fl->fl_file = file;
			err = dsfs_setlk(file, cmd, fl);
			break;

	}
	fl->fl_file = file;
	return err;
}

static ssize_t dsfs_sendfile(struct file *file, loff_t *ppos,
			      size_t count, read_actor_t actor, void *target)
{
	struct file *h_file = dsfs_h_file(file);
	int err = -EINVAL;

	BUG_ON(!h_file);
	if (h_file->f_op && h_file->f_op->sendfile)
		err = h_file->f_op->sendfile(h_file, ppos, count,
						actor, target);

	return err;
}
#endif

static ssize_t dsfs_dbg_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	char dbg_bits[24];
	int cnt;

	DSFS_DBG("");
	if (*ppos)
		return 0;
	cnt = snprintf(dbg_bits, sizeof(dbg_bits), "%08x\n", __dsfs_dbg_mask);
	if (cnt > count)
		return -ENOMEM;
	if (copy_to_user(buf, dbg_bits, cnt))
		return -EACCES;
	file->f_pos = *ppos = cnt;
	return cnt;
}

static ssize_t dsfs_dbg_write(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
	char bits[24];
	unsigned int mask;
	int cnt;
	DSFS_DBG("");
	if (*ppos)
		return -EINVAL;
	if (count >= sizeof(bits))
		return -ENOMEM;
	if (copy_from_user(bits, buf, count))
		return -EACCES;
	cnt = sscanf(bits, "%d", &mask);
	if (cnt == 1)
		__dsfs_dbg_mask = mask;
	file->f_pos = *ppos = count;
	return count;
}

struct file_operations dsfs_dbg_fops =
{
	.read = dsfs_dbg_read,
	.write = dsfs_dbg_write,
};

struct file_operations dsfs_dir_fops =
{
	.llseek =    dsfs_llseek,
	.read =      dsfs_read,
	.write =     dsfs_write,
	.readdir =   dsfs_readdir,
	.ioctl =     dsfs_ioctl,
	.mmap =      dsfs_mmap,
	.open =      dsfs_open,
	.flush =     dsfs_flush,
	.release =   dsfs_release,
	.fsync =     dsfs_fsync,
	.fasync =    dsfs_fasync,
#if 0
	.lock =      dsfs_lock,
	.sendfile =  dsfs_sendfile,
#endif
};

struct file_operations dsfs_root_fops =
{
	.read =      dsfs_read,
	.write =     dsfs_write,
	.llseek =    dsfs_llseek,
	.readdir =   dsfs_readdir,
	.ioctl =     dsfs_ioctl,
	.mmap =      dsfs_mmap,
	.open =      dsfs_open,
	.flush =     dsfs_flush,
	.release =   dsfs_release,
	.fsync =     dsfs_fsync,
	.fasync =    dsfs_fasync,
#if 0
	.lock =      dsfs_lock,
	.sendfile =  dsfs_sendfile,
#endif
};

struct file_operations dsfs_dev_list_fops =
{
	.read =      dsfs_dev_list_read,
	.write =     dsfs_dev_list_write,
	.open =      dsfs_open,
	.llseek =    dsfs_llseek,
	.readdir =   dsfs_readdir,
	.ioctl =     dsfs_ioctl,
	.mmap =      dsfs_mmap,
	.flush =     dsfs_flush,
	.release =   dsfs_release,
	.fsync =     dsfs_fsync,
	.fasync =    dsfs_fasync,
};

struct file_operations dsfs_mds_fops =
{
	.read =      dsfs_mds_read,
	.write =     dsfs_mds_write,
	.open =      dsfs_open,
	.llseek =    dsfs_llseek,
	.readdir =   dsfs_readdir,
	.ioctl =     dsfs_ioctl,
	.mmap =      dsfs_mmap,
	.flush =     dsfs_flush,
	.release =   dsfs_release,
	.fsync =     dsfs_fsync,
	.fasync =    dsfs_fasync,
};

struct file_operations dsfs_def_layout_fops =
{
	.read =      dsfs_def_lo_read,
	.write =     dsfs_def_lo_write,
	.open =      dsfs_open,
	.llseek =    dsfs_llseek,
	.readdir =   dsfs_readdir,
	.ioctl =     dsfs_ioctl,
	.mmap =      dsfs_mmap,
	.flush =     dsfs_flush,
	.release =   dsfs_release,
	.fsync =     dsfs_fsync,
	.fasync =    dsfs_fasync,
#if 0
	.lock =      dsfs_lock,
	.sendfile =  dsfs_sendfile,
#endif
};

