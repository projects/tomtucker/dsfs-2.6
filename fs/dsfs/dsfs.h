/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#ifndef __DSFS_H
#define __DSFS_H

#define DSFS_DBG(__fmt, ...) \
if (unlikely(__dsfs_dbg_mask & DSFS_DBG_BITS))	\
	printk("%s:%d " __fmt "\n",\
		__FUNCTION__, __LINE__, ## __VA_ARGS__)
#define DSFS_DBG_SUPER		0x0001
#define DSFS_DBG_INODE		0x0002
#define DSFS_DBG_EXPORT		0x0004
#define DSFS_DBG_NAMEI		0x0008
#define DSFS_DBG_LIB		0x0010
#define DSFS_DBG_DENTRY		0x0020
#define DSFS_DBG_FILE_OPS	0x0040
#define DSFS_DBG_CTL_FILE	0x0080
#define DSFS_DBG_LAYOUT_FILE	0x0100
#define DSFS_DBG_DC_INODE	0x0200
#define DSFS_DBG_DC_FILE		0x0400

/* bit vector of enabled debug information */
extern unsigned int __dsfs_dbg_mask;

/* Special file names */
#define DSFS_DEF_LAYOUT_FILE	"default_layout"
#define DSFS_MDS_FILE		"mds"
#define DSFS_DBG_FILE		"debug"
#define DSFS_DEV_LIST_FILE	"dev_list"
#define DSFS_DC_DIR		"dc"

/* Various limits */
#define DSFS_MAX_DS_HOSTNAME	128
#define DSFS_MAX_DS_EXPNAME	128
#define DSFS_MAX_DS_OPTS	128
#define DSFS_MAX_DS_PATHLEN	512
#define DSFS_MAX_DS_TYPE	16
#define DSFS_MAX_DEVNAME	(DSFS_MAX_DS_HOSTNAME+DSFS_MAX_DS_EXPNAME+2)

struct dsfs_data_server {
	int id;
	char fstype[DSFS_MAX_DS_TYPE];
	char hostname[DSFS_MAX_DS_HOSTNAME];
	char expname[DSFS_MAX_DS_EXPNAME];
	char options[DSFS_MAX_DS_OPTS];
	struct list_head ds_list;
	enum dsfs_ds_status {
		DSFS_DS_INIT,
		DSFS_DS_READY,
		DSFS_DS_ERROR,
	} status;
};

/* On disk layout */
#define DSFS_LAYOUT_MAGIC	0xc0de0000
#define DSFS_LAYOUT_VERSION	0x00000001
#define DSFS_LAYOUT_MAX_DEVS	4

struct dsfs_layout_ {
	__be32 magic;
	__be32 version;
	__be64 fsize;
	__be32 stripe_type;
	__be32 stripe_size;
	__be32 dev_count;
	__be32 dev_list[DSFS_LAYOUT_MAX_DEVS];
};

/* On disk device list */
#define DSFS_DEV_LIST_MAGIC	0xd54f5de7
#define DSFS_DEV_LIST_VERSION	0x00000001
#define DSFS_DEV_LIST_MAX_NAME	32

struct dsfs_dev_name_ {
	__be32 dev_id;
	char dev_name[DSFS_DEV_LIST_MAX_NAME];
};

struct dsfs_dev_list_ {
	__be32 magic;
	__be32 version;
	__be32 dev_count;
	struct dsfs_dev_name_ dev_list[DSFS_LAYOUT_MAX_DEVS];
};

struct dsfs_dev_name {
	int dev_id;
	char dev_name[DSFS_DEV_LIST_MAX_NAME];
};
struct dsfs_dev_list {
	int dev_count;
	struct dsfs_dev_name dev_list[DSFS_LAYOUT_MAX_DEVS];
};

/* In memory layout */
struct dsfs_layout {
	u32 version;
	enum {
		DSFS_ROUND_ROBIN,
		DSFS_STRIPE_DENSE,
		DSFS_STRIPE_SPARSE,
	} stripe_type;
	size_t stripe_size;
	unsigned long long fsize;
	size_t dev_count;
	int dev_list[DSFS_LAYOUT_MAX_DEVS];
	struct file *dev_file[DSFS_LAYOUT_MAX_DEVS];
};
#define DSFS_STRIPE_DEF_SZ	65536
 
struct dsfs_inode {
	struct inode ii_inode;
	enum dsfs_inode_type {
		DSFS_I_MDS_CNTL, /* MDS_LIST */
		DSFS_I_DEV_CNTL, /* DEV_LIST */
		DSFS_I_LO_CNTL,  /* DEFAULT_LAYOUT */
		DSFS_I_DS,	  /* File in a DS mnt */
		DSFS_I_ROOT,	  /* File in root dir */
		DSFS_I_DC,	  /* File in DC dir */
	} ii_type;
	struct inode *ii_h_inode; /* Host FS inode (file contains layout) */
	struct dsfs_layout *ii_layout;

	/* Array of dentry for each DS (populated by lookup) */
	struct dentry *ii_dev_list[DSFS_LAYOUT_MAX_DEVS];
};
static inline struct dsfs_inode *DSFS_I(struct inode *__i)
{
	return container_of(__i, struct dsfs_inode, ii_inode);
}
static inline struct inode *dsfs_h_inode(struct inode *__i)
{
	return DSFS_I(__i)->ii_h_inode;
}

/* 
 * DSFS dentry.  We hold a reference on the host VFS dentry and mount
 * point when set.
 */
struct dsfs_dentry {
	struct dentry *di_h_dentry;
	struct vfsmount *di_h_mnt;
};
static struct dsfs_dentry *DSFS_D(struct dentry *d)
{
	return (struct dsfs_dentry *)d->d_fsdata;
}
static inline struct dentry *dsfs_h_dentry(struct dentry *d)
{
	BUG_ON(!DSFS_D(d));
	return DSFS_D(d)->di_h_dentry;
}
static inline struct vfsmount *dsfs_h_mnt(struct dentry *d)
{
	BUG_ON(!DSFS_D(d));
	return DSFS_D(d)->di_h_mnt;
}

#define DSFS_ROLE_MDS	1
#define DSFS_ROLE_DS	2

struct dsfs_sb {
	enum {
		DSFS_INIT,
		DSFS_READY,
		DSFS_PAUSED,
	} si_state;
	unsigned int si_mnt_opts;
	char *si_rootpath;		/* path to host storage */
	struct super_block *si_sb;	/* back ptr to my sb */
	struct super_block *si_h_sb;	/* host fs sb */
	spinlock_t si_ds_lock;
	struct dsfs_layout *si_def_lo;
	struct dsfs_dev_list *si_dev_list;
	struct dentry *si_dc_root;
};
static inline struct dsfs_sb *DSFS_SB(struct super_block *sb)
{
	return sb->s_fs_info;
}

struct dsfs_file {
	struct file *fi_h_file;
};
static inline struct dsfs_file *DSFS_F(struct file *f)
{
	return (struct dsfs_file *)f->private_data;
}

static inline struct file *dsfs_h_file(struct file *f)
{
	return (DSFS_F(f)?DSFS_F(f)->fi_h_file:NULL);
}

struct dsfs_nd_args {
	struct dentry *dentry;
	struct vfsmount *mnt;
};

/*
 * Save the dentry and mnt fields of the nameidata structure and replace
 * them with the host dentry and mount values from the nd dentry
 */
static inline void dsfs_nd_push(struct dsfs_nd_args *sa, struct nameidata *nd)
{
	if (!nd)
		return;

	/* save current values */
	sa->dentry = nd->path.dentry;
	sa->mnt = nd->path.mnt;

	/* replace with host values */
	nd->path.dentry = dsfs_h_dentry(sa->dentry);
	nd->path.mnt = dsfs_h_mnt(sa->dentry);
}

static inline void dsfs_nd_push_(struct dsfs_nd_args *sa, struct nameidata *nd,
				  struct dentry *d__, struct vfsmount *m__)
{
	if (!nd)
		return;

	/* save current values */
	sa->dentry = nd->path.dentry;
	sa->mnt = nd->path.mnt;

	/* replace with host values */
	nd->path.dentry = d__;
	nd->path.mnt = m__;
}

/*
 * Restore the nameidata structure's dentry and mnt fields from
 * the save args area
 */
static inline void dsfs_nd_pop(struct dsfs_nd_args *sa, struct nameidata *nd)
{
	if (!nd)
		return;

	nd->path.dentry = sa->dentry;
	nd->path.mnt = sa->mnt;
}

extern struct inode *dsfs_iget(struct super_block *sb, unsigned long ino);
extern int dsfs_inode_attach(struct dentry *h_dentry, struct dentry *dentry,
			      struct super_block *sb, struct inode **ppi);
extern int dsfs_dentry_attach(struct dentry *h_dentry, struct dentry *dentry);

struct inode *dsfs_get_inode(struct super_block *sb, int mode, dev_t dev);
extern int dsfs_get_sb(struct file_system_type *fs_type, int flags, const char *dev_name, void *data, struct vfsmount *mnt);
extern struct dentry *dsfs_get_parent(struct dentry *child);

extern struct vm_operations_struct generic_file_vm_ops;
extern struct dsfs_export *dsfs_alloc_export(void);

extern const struct address_space_operations dsfs_aops;
extern const struct file_operations dsfs_file_operations;
extern const struct inode_operations dsfs_file_inode_operations;
extern const struct inode_operations dsfs_mds_dir_inode_ops ;
extern const struct inode_operations dsfs_dir_inode_operations;
extern struct export_operations dsfs_export_ops;

/* convenience macros */
#define DSFS_MAGIC 0x4E445346
#define DSFS_VERSION 1

extern void dsfs_copy_attr_times(struct inode *dst, const struct inode *src);
extern void dsfs_copy_attr_timesizes(struct inode *dst, const struct inode *src);
extern void dsfs_copy_attr_all(struct inode *dst, const struct inode *src);
extern void dsfs_dc_attr_timesizes(struct inode *dst, const struct inode *src);
extern void dsfs_dc_copy_attr_all(struct inode *dst, const struct inode *src);
extern int dsfs_read_dev_list(struct inode *parent, struct dentry *dentry, struct inode *inode);
extern ssize_t dsfs_dev_list_read(struct file *file, char *buf, size_t count, loff_t *ppos);
extern ssize_t dsfs_dev_list_write(struct file *file, const char *buf, size_t count, loff_t *ppos);
extern ssize_t dsfs_mds_read(struct file *file, char *buf, size_t count, loff_t *ppos);
extern ssize_t dsfs_mds_write(struct file *file, const char *buf, size_t count, loff_t *ppos);
extern int dsfs_dev_list_init(struct dentry *root);
extern int dsfs_read_layout(struct dentry *dentry, struct inode *inode);
extern int dsfs_layout_init(struct dentry *root);
extern int dsfs_write_layout_kern(struct file *filp, struct dsfs_layout *lo);
extern int dsfs_read_layout_kern(struct file *filp, struct dsfs_layout *lo);

/* inode operation vectors */
extern struct inode_operations dsfs_symlink_iops;
extern struct inode_operations dsfs_dir_iops;
extern struct inode_operations dsfs_root_iops;
extern struct inode_operations dsfs_dc_dir_iops;
extern struct inode_operations dsfs_ctl_iops;

/* file opertation vectors */
extern struct file_operations dsfs_root_fops;
extern struct file_operations dsfs_dc_fops;
extern struct file_operations dsfs_dir_fops;
extern struct file_operations dsfs_dev_list_fops;
extern struct file_operations dsfs_mds_fops;
extern struct file_operations dsfs_def_layout_fops;
extern struct file_operations dsfs_dbg_fops;

/* dentry operation vectors */ 
extern struct dentry_operations dsfs_dops;

/* Local content inode methods */
int dsfs_mkdir(struct inode *dir, struct dentry *dentry, int mode);
int dsfs_create(struct inode *dir, struct dentry *dentry, int mode, struct nameidata *nd);
struct dentry *dsfs_lookup(struct inode *dir, struct dentry *dentry, struct nameidata* nd);

/* Striped content inode methods */
int dsfs_dc_mkdir(struct inode *dir, struct dentry *dentry, int mode);
int dsfs_dc_rmdir(struct inode *dir, struct dentry *dentry);
int dsfs_dc_create(struct inode *dir, struct dentry *dentry, int mode, struct nameidata *nd);
int dsfs_dc_unlink(struct inode *dir, struct dentry *dentry);
struct dentry *dsfs_dc_lookup(struct inode *dir, struct dentry *dentry, struct nameidata* nd);

/* Utility functions */
int dsfs_def_layout_init(struct dentry *root);
ssize_t dsfs_def_lo_write(struct file *file, const char __user *buf,
			   size_t count, loff_t *ppos);
ssize_t dsfs_def_lo_read(struct file *file, char __user *buf,
			  size_t count, loff_t *ppos);
struct file *dsfs_open_file(struct dsfs_sb *sbi, char *fname);
int dsfs_set_state(struct dsfs_sb *sbi, int new_state);

static inline struct dentry *dsfs_lock_parent(struct dentry *dentry)
{
	struct dentry *dir = dget(dentry->d_parent);
	mutex_lock(&dir->d_inode->i_mutex);
	return dir;
}

static inline void unlock_dir(struct dentry *dir)
{
	mutex_unlock(&dir->d_inode->i_mutex);
	dput(dir);
}

#endif
