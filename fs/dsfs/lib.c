/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/version.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/exportfs.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/file.h>
#include <linux/mm_types.h>
#include <linux/smp_lock.h>
#include <linux/uaccess.h>
#include <linux/pagemap.h>
#include <linux/init.h>
#include <linux/ctype.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_LIB

static void __copy_attr_times(struct inode *dst, const struct inode *src)
{
	dst->i_atime = src->i_atime;
	dst->i_mtime = src->i_mtime;
	dst->i_ctime = src->i_ctime;
}

static void __copy_attr_size(struct inode *dst, const struct inode *src)
{
	i_size_write(dst, i_size_read((struct inode *)src));
	dst->i_blkbits = src->i_blkbits;
	dst->i_blocks = src->i_blocks;
}

static void __copy_attr_access(struct inode *dst, const struct inode *src)
{
	dst->i_mode = src->i_mode;
	dst->i_nlink = src->i_nlink;
	dst->i_uid = src->i_uid;
	dst->i_gid = src->i_gid;
	dst->i_rdev = src->i_rdev;
	dst->i_flags = src->i_flags;
}

void dsfs_copy_attr_times(struct inode *dst, const struct inode *src)
{
	__copy_attr_times(dst, src);
}

void dsfs_copy_attr_timesizes(struct inode *dst, const struct inode *src)
{
	__copy_attr_times(dst, src);
	__copy_attr_size(dst, src);
}

void dsfs_copy_attr_all(struct inode *dst, const struct inode *src)
{
	__copy_attr_times(dst, src);
	__copy_attr_access(dst, src);
	__copy_attr_size(dst, src);
}

void dsfs_dc_copy_attr_timesizes(struct inode *dst, const struct inode *src)
{
	__copy_attr_times(dst, src);
	i_size_write(dst, DSFS_I(dst)->ii_layout->fsize);
	dst->i_blkbits = src->i_blkbits;
	dst->i_blocks = src->i_blocks;
}

void dsfs_dc_copy_attr_all(struct inode *dst, const struct inode *src)
{
	__copy_attr_times(dst, src);
	__copy_attr_access(dst, src);
	i_size_write(dst, DSFS_I(dst)->ii_layout->fsize);
	dst->i_blkbits = src->i_blkbits;
	dst->i_blocks = src->i_blocks;
}

struct file *dsfs_open_file(struct dsfs_sb *sbi, char *fname)
{
	struct file *filp;
	char path[DSFS_MAX_DS_PATHLEN];

	sprintf(path, "%s/%s", sbi->si_rootpath, fname);
	DSFS_DBG("path=%s", path);
	filp = filp_open(path, O_RDWR, 0);
	if (IS_ERR(filp))
		return filp;
	if (!filp->f_op || !filp->f_op->write) {
		filp_close(filp, current->files);
		return ERR_PTR(-EINVAL);
	}
	return filp;
}
