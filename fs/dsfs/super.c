/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/exportfs.h>
#include <linux/vmalloc.h>
#include <linux/mount.h>
#include <linux/pagemap.h>
#include <linux/init.h>
#include <linux/namei.h>
#include <linux/parser.h>
#include <linux/ctype.h>
#include <linux/seq_file.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_SUPER

unsigned int __dsfs_dbg_mask;

static struct kmem_cache *dsfs_inode_cachep;
static atomic_t dsfs_count_active_inodes;

/*
 * Initialize an inode cache slab element prior to any use
 */
static void dsfs_i_init_once(struct kmem_cache *cachep, void *_inode)
{
	struct dsfs_inode *di = _inode;

	di->ii_type = DSFS_I_ROOT;
	di->ii_h_inode = NULL;
	inode_init_once(&di->ii_inode);
}

/*
 * Allocate a DSFS inode struct from our slab cache
 */
static struct inode *dsfs_alloc_inode(struct super_block *sb)
{
	struct dsfs_inode *inode;
	inode = kmem_cache_alloc(dsfs_inode_cachep, GFP_KERNEL);
	if (!inode)
		return NULL;

	atomic_inc(&dsfs_count_active_inodes);
	inode->ii_inode.i_version = DSFS_VERSION;
	inode->ii_layout = NULL;
	memset(inode->ii_dev_list, 0, sizeof(inode->ii_dev_list));
	DSFS_DBG("inode %p", inode);
	return &inode->ii_inode;
}

/*
 * Destroy a DSFS inode struct
 */
static void dsfs_destroy_inode(struct inode *_inode)
{
	struct dsfs_inode *inode = DSFS_I(_inode);
	DSFS_DBG("_inode %p", _inode);
	kmem_cache_free(dsfs_inode_cachep, inode);
	atomic_dec(&dsfs_count_active_inodes);
}

static int dsfs_write_inode(struct inode *inode, int sync)
{
	DSFS_DBG("inode=%p, sync=%d", inode, sync);
	return 0;
}

static void dsfs_drop_inode(struct inode *inode)
{
	struct inode *h_inode = dsfs_h_inode(inode);
	DSFS_DBG("inode %p h_inode %p", inode, h_inode);
	if (h_inode->i_sb->s_op->drop_inode)
		h_inode->i_sb->s_op->drop_inode(h_inode);
	generic_drop_inode(inode);
}

static void dsfs_delete_inode(struct inode *inode)
{
	DSFS_DBG("inode=%p", inode);
	truncate_inode_pages(&inode->i_data, 0);
	clear_inode(inode);
}

static void dsfs_kill_litter_super(struct super_block *sb)
{
	DSFS_DBG("sb %p", sb);
	(void)dsfs_set_state(DSFS_SB(sb), DSFS_INIT);
	kill_litter_super(sb);
}

static void dsfs_put_super(struct super_block *sb)
{
	DSFS_DBG("sb = %p, sb->s_fs_info=%p, rootpath=%s",
		sb, sb->s_fs_info, DSFS_SB(sb)->si_rootpath);
	kfree(DSFS_SB(sb)->si_rootpath);
	kfree(DSFS_SB(sb));
}

static int dsfs_statfs(struct dentry *dentry, struct kstatfs *buf)
{
	DSFS_DBG("");
	return vfs_statfs(dsfs_h_dentry(dentry), buf);
}

static int dsfs_remount_fs(struct super_block *sb, int *flags, char *data)
{
	DSFS_DBG("");
	return -ENOSYS;
}

static void dsfs_clear_inode(struct inode *inode)
{
	DSFS_DBG("inode = %p", inode);
	iput(dsfs_h_inode(inode));
}

static void dsfs_write_super(struct super_block  *sb)
{
	struct super_block *h_sb = DSFS_SB(sb)->si_h_sb;
	DSFS_DBG("");
	if (h_sb->s_op->write_super)
		h_sb->s_op->write_super(h_sb);
}

static void dsfs_umount_begin(struct super_block *sb)
{
	struct dsfs_sb *sbi = DSFS_SB(sb);
	struct super_block *h_sb = sbi->si_h_sb;
	DSFS_DBG("");
	if (h_sb->s_op->umount_begin)
		h_sb->s_op->umount_begin(h_sb);
}

static int dsfs_show_options(struct seq_file *m, struct vfsmount *mnt)
{
	struct super_block *sb = mnt->mnt_sb;

	seq_printf(m, ",root=%s", DSFS_SB(sb)->si_rootpath);
	seq_printf(m, ",mntopts=%x", DSFS_SB(sb)->si_mnt_opts);

	return 0;
}

static struct super_operations dsfs_sops =
{
	.alloc_inode = dsfs_alloc_inode,
	.destroy_inode = dsfs_destroy_inode,
	.write_inode = dsfs_write_inode,
	.drop_inode = dsfs_drop_inode,
	.delete_inode = dsfs_delete_inode,
	.put_super = dsfs_put_super,
	.statfs = dsfs_statfs,
	.remount_fs = dsfs_remount_fs,
	.clear_inode = dsfs_clear_inode,
	.umount_begin = dsfs_umount_begin,
	.write_super = dsfs_write_super,
	.show_options = dsfs_show_options,
};

enum {
	OPT_MDS_ROLE, OPT_DS_ROLE, OPT_BOTH_ROLE, OPT_ROOT, OPT_DEBUG
};

static match_table_t tokens = {
	{OPT_MDS_ROLE, "role=mds"},
	{OPT_DS_ROLE, "role=ds"},
	{OPT_BOTH_ROLE, "role=both"},
	{OPT_ROOT, "root=%s"},
	{OPT_DEBUG, "debug=%d"},
};

static int parse_options (char *options, struct dsfs_sb *sbi)
{
	char * p;
	substring_t args[MAX_OPT_ARGS];
	int dbg;

	DSFS_DBG("options=%p, sbi=%p", options, sbi);
	if (!options)
		return 1;

	DSFS_DBG("options='%s'", options);

	while ((p = strsep (&options, ",")) != NULL) {
		int token;
		if (!*p)
			continue;

		token = match_token(p, tokens, args);
		switch (token) {
		case OPT_BOTH_ROLE:
		case OPT_MDS_ROLE:
			sbi->si_mnt_opts |= DSFS_ROLE_MDS;
			sbi->si_mnt_opts |= DSFS_ROLE_DS;
			break;
		case OPT_DS_ROLE:
			sbi->si_mnt_opts |= DSFS_ROLE_DS;
			break;
		case OPT_ROOT:
			sbi->si_rootpath = match_strdup(&args[0]);
			if (!sbi->si_rootpath) {
				printk(KERN_ERR
					"DSFS: not enough memory for "
					"root path name.\n");
				return 0;
			}
			break;
		case OPT_DEBUG:
			if (!match_int(&args[0], &dbg))
				__dsfs_dbg_mask = dbg;
			break;
		default:
			printk (KERN_ERR
				"DSFS: Unrecognized mount option \"%s\" "
				"or missing value\n", p);
			return 0;
		}
	}
	if (!sbi->si_rootpath) {
		printk(KERN_ERR
		       "DSFS: you must specify a root path\n");
		return -ENOENT;
	}

	return 1;
}

static int dsfs_create_file(struct dentry *root, const char *fname, int mode,
			     enum dsfs_inode_type type,
			     struct file_operations *fops,
			     struct inode_operations *iops)
{
	struct dentry *dentry;
	int err;
	DSFS_DBG("fname=%s", fname);

	/* Lookup the file's dentry */
	dentry = lookup_one_len(fname, root, strlen(fname));
	if (IS_ERR(dentry)) {
		err = PTR_ERR(dentry);
		goto out;
	}
	DSFS_DBG("dentry=%p, DSFS_D(dentry)=%p, dentry->d_inode=%p",
		dentry, DSFS_D(dentry), dentry->d_inode);

	/* See if the file already exists. If not, create it */
	if (!dentry->d_inode) {
		err = dsfs_create(root->d_inode, dentry, mode, NULL);
		if (err)
			goto out;
	}
	DSFS_I(dentry->d_inode)->ii_type = type;
	if (iops)
		dentry->d_inode->i_op = iops;
	if (fops)
		dentry->d_inode->i_fop = fops;
	err = 0;
	DSFS_DBG("dentry->d_inode = %p, fops = %p",
		dentry->d_inode, fops);
 out:
	return err;
}

int dsfs_create_dir(struct dentry *root, const char *dirname,
		    enum dsfs_inode_type type, int mode,
		    struct file_operations *fops,
		    struct inode_operations *iops)
{
	struct dentry *dentry;
	int err = 0;
	DSFS_DBG("dirname=%s", dirname);

	/* Lookup the directory's */
	dentry = lookup_one_len(dirname, root, strlen(dirname));
	if (IS_ERR(dentry)) {
		err = PTR_ERR(dentry);
		goto out;
	}

	/* See if the directory already exists. If not, create it */
	if (!dentry->d_inode) {
		err = dsfs_mkdir(root->d_inode, dentry, mode);
		if (err)
			goto out;
	}
	DSFS_I(dentry->d_inode)->ii_type = type;
	if (fops)
		dentry->d_inode->i_fop = fops;
	if (iops)
		dentry->d_inode->i_op = iops;
 out:
	return err;
}

static int dsfs_fill_super(struct super_block * sb, void * data, int silent)
{
	struct nameidata nd;
	struct dentry *root;
	struct dsfs_sb *sbi;
	int err = -EINVAL;
	DSFS_DBG("sb=%p, data=%p, silent=%d", sb, data, silent);

	/* Allocate our private super block structure */
	sbi = kzalloc(sizeof *sbi, GFP_KERNEL);
	if (!sbi)
		return -ENOMEM;
	sb->s_fs_info = sbi;
	sbi->si_sb = sb;
	spin_lock_init(&sbi->si_ds_lock);
	sbi->si_state = DSFS_INIT;

	/* Parse the mount options and set them in our superblock data */
	if (!data)
		goto err0;
	err = parse_options(data, sbi);
	if (err < 0)
		goto err0;

	/* Look up the path provided for the host vfs */
	DSFS_DBG("VFS path: %s", sbi->si_rootpath);
	err = path_lookup(sbi->si_rootpath, LOOKUP_FOLLOW, &nd);
	if (err) {
		printk(KERN_INFO "DSFS: error mounting rootfs path %s\n",
		       sbi->si_rootpath);
		goto err0;
	}
	/* Make sure the path terminates with an inode */
	if (!nd.path.dentry->d_inode) {
		printk(KERN_INFO "DSFS: bad rootpath, no inode %s\n",
		       sbi->si_rootpath);
		goto err1;
	}
	/* Make sure host fs is exportable */
	sbi->si_h_sb = nd.path.dentry->d_sb;
	if (!sbi->si_h_sb->s_export_op) {
		printk(KERN_INFO
		       "DSFS: specified host FS is not exportable %s\n",
		       sbi->si_rootpath);
		err = -ENOTSUPP;
		goto err1;
	}
	sb->s_export_op = &dsfs_export_ops;
	sb->s_maxbytes = sbi->si_h_sb->s_maxbytes;
	sb->s_op = &dsfs_sops;

	sb->s_blocksize = PAGE_CACHE_SIZE;
	sb->s_blocksize_bits = PAGE_CACHE_SHIFT;
	sb->s_magic = DSFS_MAGIC;
	sb->s_time_gran = 1;

	/* Allocate the root dentry */
	err = -ENOMEM;
	root = d_alloc(NULL, &(const struct qstr){hash: 0, name: "/", len: 1});
	if (IS_ERR(root))
		goto err1;

	root->d_sb = sb;
	root->d_op = &dsfs_dops;
	root->d_parent = root;
	sb->s_root = root;

	err = -ENOMEM;
	root->d_fsdata = kzalloc(sizeof(struct dsfs_dentry), GFP_KERNEL);
	if (!root->d_fsdata)
		goto err2;

	/* Attach the host vfs dentry and mnt pt. to our root dentry */
	DSFS_D(root)->di_h_dentry = nd.path.dentry;
	DSFS_D(root)->di_h_mnt = nd.path.mnt;
	DSFS_DBG("DSFS_D->dentry=%p, DSFS_D->mnt=%p",
		DSFS_D(root)->di_h_dentry, DSFS_D(root)->di_h_mnt);

	err = dsfs_inode_attach(nd.path.dentry, root, sb, NULL);
	if (err)
		goto err3;

	/* Create the debug control file */
	err = dsfs_create_file(root, DSFS_DBG_FILE, 0644,
				DSFS_I_ROOT,
				&dsfs_dbg_fops, &dsfs_ctl_iops);
	if (err)
		goto err3;

	/* Initialize the layout */
	err = dsfs_def_layout_init(root);
	if (err)
		goto err3;

	/* Initialize the DS_LIST control file */
	err = dsfs_dev_list_init(root);
	if (err)
		goto err3;

	/* Create the MDS file */
	err = dsfs_create_file(root, DSFS_MDS_FILE, 0644,
				DSFS_I_MDS_CNTL,
				&dsfs_mds_fops, &dsfs_ctl_iops);
	if (err)
		goto err3;

	/* Create the DC distributed content directory */
	err = dsfs_create_dir(root, DSFS_DC_DIR, DSFS_I_DC, 0755,
			      NULL, &dsfs_dc_dir_iops);
	if (err)
		goto err3;

	DSFS_DBG("root->d_count=%d", atomic_read(&root->d_count));
	return 0;

 err3:
	if (sbi->si_dev_list)
		kfree(sbi->si_dev_list);
	if (sbi->si_def_lo)
		kfree(sbi->si_def_lo);
	kfree(root->d_fsdata);
 err2:
	DSFS_DBG("root->d_count=%d", atomic_read(&root->d_count));
	dput(root);
 err1:
	dput(nd.path.dentry);
	mntput(nd.path.mnt);
 err0:
	kfree(sbi);
	return err;
}

int dsfs_get_sb(struct file_system_type *fs_type,
	int flags, const char *dev_name, void *data, struct vfsmount *mnt)
{
	DSFS_DBG("fs_type=%p, flags=%x, dev_name=%s, data=%p, mnt=%p",
		fs_type, flags, dev_name, data, mnt);
	return get_sb_nodev(fs_type, flags, data, dsfs_fill_super, mnt);
}

static struct file_system_type dsfs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= "dsfs",
	.get_sb		= dsfs_get_sb,
	.kill_sb	= dsfs_kill_litter_super,
};

static int __init init_dsfs(void)
{
	int ret;

	printk(KERN_INFO "dsfs: Registering 'dsfs' filesystem...");

	/* Create inode cache */
	ret = -ENOMEM;
	atomic_set(&dsfs_count_active_inodes, 0);
	dsfs_inode_cachep = kmem_cache_create("dsfs_inode_cache",
					       sizeof(struct dsfs_inode),
					       0,
					       SLAB_HWCACHE_ALIGN,
					       dsfs_i_init_once);
	if (!dsfs_inode_cachep) {
		printk(KERN_INFO
		       "FAILED could not allocate inode cache.\n");
		return ret;
	}

	ret = register_filesystem(&dsfs_fs_type);
	if (ret) {
		printk(KERN_INFO
		       "FAILED could not register fs type ret=%d.\n", ret);
		kmem_cache_destroy(dsfs_inode_cachep);
	} else
		printk("OK.\n");

	return ret;
}

static void __exit exit_dsfs(void)
{
	printk(KERN_INFO "dsfs: Unregistering 'dsfs'");
	unregister_filesystem(&dsfs_fs_type);

	if (atomic_read(&dsfs_count_active_inodes) != 0) {
		printk("dsfs: %d active inode objects still present\n",
		       atomic_read(&dsfs_count_active_inodes));
	}

	kmem_cache_destroy(dsfs_inode_cachep);
}

MODULE_AUTHOR("Tom Tucker");
MODULE_DESCRIPTION("Data Server Network File System for pNFS");
MODULE_LICENSE("Dual BSD/GPL");
module_init(init_dsfs)
module_exit(exit_dsfs)
