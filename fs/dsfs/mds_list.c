/*
 * Copyright (c) 2008 Open Grid Computing, Inc. All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directory of this source tree, or the BSD-type
 * license below:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *      Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *      Neither the name of the Network Appliance, Inc. nor the names of
 *      its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Tom Tucker <tom@opengridcomputing.com>
 */
#include <linux/fs.h>
#include <linux/exportfs.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/file.h>
#include <linux/mm_types.h>
#include <linux/smp_lock.h>
#include "dsfs.h"
#define DSFS_DBG_BITS DSFS_DBG_MDS_LIST

#define MDS_FILE_HDR "# mds_num hostname fstype options\n"
#define MDS_FILE_HDR_LEN (sizeof(MDS_FILE_HDR)-1)
/*
 * This logic filters writes to the MDS_LIST file in order to add data
 * servers to the system.
 */
ssize_t mds_list_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	int err = -EINVAL;
	struct file *h_file;

	DSFS_DBG("file=%p, buf=%p, count=%zu, ppos=%p", file, buf, count, ppos);

	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);

	if (!h_file->f_op || !h_file->f_op->read)
		goto out;

	if (count < sizeof(MDS_FILE_HDR_LEN))
		return -EINVAL;

	memcpy(buf, MDS_FILE_HDR, MDS_FILE_HDR_LEN);
	*ppos += MDS_FILE_HDR_LEN;
	count -= MDS_FILE_HDR_LEN;
	err = h_file->f_op->read(h_file, buf+MDS_FILE_HDR_LEN,
				 count, ppos);
	if (err >= 0)
		file->f_dentry->d_inode->i_atime =
			h_file->f_dentry->d_inode->i_atime;
	file->f_pos = *ppos;
	memcpy(&(file->f_ra), &(h_file->f_ra), sizeof(struct file_ra_state));

out:
	return err;
}

ssize_t mds_list_write(struct file *file, const char *buf, size_t count,
		       loff_t *ppos)
{
	int err = -ENOSYS;
	struct file *h_file;
	struct inode *inode;
	struct inode *h_inode;
	loff_t pos = *ppos;

	DSFS_DBG("file=%p, buf=%p, count=%zu, *ppos=%llu",
		file, buf, count, *ppos);
	h_file = dsfs_h_file(file);
	BUG_ON(!h_file);

	inode = file->f_dentry->d_inode;
	h_inode = dsfs_h_inode(inode);

	if (count) {
		if (!h_file->f_op || !h_file->f_op->write)
			goto out;

		/* adjust for append -- seek to the end of the file */
		if ((file->f_flags & O_APPEND))
			pos = i_size_read(inode);

		err = h_file->f_op->write(h_file, buf, count, &pos);
		if (err >= 0) {
			*ppos = pos;
			dsfs_copy_attr_times(inode, h_inode);
			file->f_pos = h_file->f_pos = pos;
		}
	}

	/* update this inode's size */
	if (pos > i_size_read(inode))
		i_size_write(inode, pos);
 out:
	return err;
}

